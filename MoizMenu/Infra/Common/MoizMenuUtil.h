//
//  MoizMenuUtil.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/4/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SCNetworkReachability.h>
#import <netinet/in.h>
#import "MoizMenuUtil.h"

@interface MoizMenuUtil : NSObject

+ (CGFloat)windowHeight;
+ (NSString *)applicationDocumentsDirectory;
+ (BOOL)hasInternetConnection;
+ (UILabel*)titleViewWithTitleOnly:(NSString*)strTitle;
+ (NSNumber *)convertStringToNumber:(NSString *)strNumber;
+ (NSData *)converBase64toImage:(NSString *)stringImage;
+ (NSData *)dataFromBase64EncodedString:(NSString *)string;
+ (NSDecimalNumber *)convertStringToDecimal:(NSString *)strNumber;
+ (NSMutableURLRequest *)getAuthenticatedRequestWithURL:(NSURL *)url;
+ (NSData *)getImageFromURL:(NSString *)url;
- (void)downloadSmallProfileImageAsyncFromURL:(NSURL*)url;
+ (NSString *)getUniqueId;
+ (NSData *)getImageFromURLLink:(NSURL *)url;

@end
