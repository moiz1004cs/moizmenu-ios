//
//  User.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/25/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * profile;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * pictureUrl;
@end
