//
//  MapViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/7/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "MapViewController.h"
#import "AppDelegate.h"
#import "Restaurant.h"
#import "MoizMenuStyle.h"
#import "MoizMenuUtil.h"
#import "AFNetworking.h"
#import "MapAnnotation.h"
#import "FilterViewController.h"
#define MAP_KEY     @"AIzaSyCQf3H1rRiO6066DibQll96yUgKh6hvftM"
#define MAP_APPLE   @"Apple Map"
#define MAP_GOOGLE  @"Google Map"
#define ZOOM_LEVEL  15

@interface MapViewController (){
    
    CLLocationManager *_locationManager;
    NSMutableArray *_arrayOfNearbyRestaurant;
    
    CLLocationCoordinate2D mapLocation;
}
@property (nonatomic, strong) NSMutableArray *arrPlaceMarks;
@property (nonatomic, strong) NSMutableArray *arrayDict;
@property (nonatomic, strong) NSArray* fetchedRecordsArray;
@property (nonatomic, strong) NSMutableDictionary * addressDictionary;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) CLGeocoder *geocoder;
@property (nonatomic, strong) MKMapItem *selectedMapItem;

@property (nonatomic, strong) NSString *restoName;
@property (nonatomic, strong) NSString *restoAdd;
@property (nonatomic, strong) NSString *restoImg;
@property CLLocationCoordinate2D coords;

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arrPlaceMarks = [[NSMutableArray alloc] initWithCapacity:0];
        _arrayDict = [[NSMutableArray alloc] initWithCapacity:0];
//        [self.navigationController setNavigationBarHidden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    self.managedObjectContext = appDelegate.managedObjectContext;
    self.fetchedRecordsArray = [[appDelegate getAllRestaurant]mutableCopy];

    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    CLLocation *location = [_locationManager location];
    
    _addressDictionary = [[NSMutableDictionary alloc]init];

    mapLocation = _locationManager.location.coordinate;
//
    NSLog(@"User's location: %@", location);
    _gMapView.settings.myLocationButton = YES;
    _gMapView.myLocationEnabled = YES;
    // Create a GMSCameraPosition that tells the map to display the the coordinate
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:14.5573
                                                            longitude:121.02200
                                                                 zoom:ZOOM_LEVEL];
    _gMapView = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
//    _gMapView.delegate = self;
    [self.view insertSubview:_gMapView atIndex:1];
    

    [self.view insertSubview:_nameField atIndex:2];
    [self.view insertSubview:_cuisineField atIndex:1];
    [self.view insertSubview:_priceRangeField atIndex:1];
    [self.view insertSubview:_filterButton atIndex:1];
    
    for(Restaurant * resto in self.fetchedRecordsArray){
        NSString *strAddress = [NSString stringWithFormat:@"%@, %@, %@, %@", resto.street, resto.city, resto.state, resto.zipcode];
        [_addressDictionary setValue:strAddress forKey:@"address"];
        [_addressDictionary setValue:resto.name forKey:@"restoName"];
        [_addressDictionary setValue:resto.image forKey:@"image"];
        
        
        [self geocodeLocationWithAddress:_addressDictionary];
    }
    [self.filterButton addTarget:self action:@selector(goToFilter) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//FUCTIONS FROM NYLIFE
- (void)geocodeLocateWithAddressOrCustom:(NSString *) customAddress isCurrent:(BOOL) isCurrent{
    
    
    if (isCurrent){
        //Method to zoom map, update global declaration of location
        mapLocation = _locationManager.location.coordinate;
        
        //  DLog(@"***MAP CURRENT_LOCATION_LAT: %f", mapLocation.latitude);
        //  DLog(@"***MAP CURRENT_LOCATION_LONG: %f", mapLocation.longitude);
        self.gMapView.camera = [GMSCameraPosition cameraWithTarget:mapLocation zoom:ZOOM_LEVEL];
    }else{
        //geocode, zoom to map, update global declaration of location
        
        [self.geocoder geocodeAddressString:customAddress completionHandler:^(NSArray* placemarks, NSError* error){
            
            if ([placemarks count] > 0){
                CLPlacemark *placemark = [placemarks lastObject];
                //zoom to map. update global declaration
                mapLocation = placemark.location.coordinate;
                
                // DLog(@"***MAP CUSTOM_LOCATION_LAT: %f", mapLocation.latitude);
                // DLog(@"***MAP CUSTOM_LOCATION_LONG: %f", mapLocation.longitude);
                self.gMapView.camera = [GMSCameraPosition cameraWithTarget:mapLocation zoom:ZOOM_LEVEL];
                
                
            }else{
                //Prompt that location cannot be geocoded
                
                NSLog(@"Map Error : %@", error);
                
                if ([MoizMenuUtil hasInternetConnection]) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Geocode Error"
                                                                        message:@"Error on GeOcode"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"Ok"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }else{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Geocode Error"
                                                                        message:@"No internet connection"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"Ok"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
                
                
            }
            
        }];
    }
    
}

-(void) geocodeLocationWithAddress:(NSDictionary *) dict{
    

        NSString *address = [dict objectForKey:@"address"];
        NSString *restoName = [dict objectForKey:@"restoName"];
        NSString *restoImage = [dict objectForKey:@"image"];
    
    
    
    if(address){
        self.geocoder = nil;
        [self.geocoder geocodeAddressString:address completionHandler:^(NSArray* placemarks, NSError* error){
        
            if ([placemarks count] > 0){
                CLPlacemark *placemark = [placemarks objectAtIndex:0];

                NSLog(@"Resto Name: %@", restoName);
                NSLog(@"Address: %@", address);
                
//                _restoName = restoName;
//                _restoAdd = address;
//                _restoImg = restoImage;

                [self plotSelectedRestaurantOnMap:placemark.location andAddress:address andRestoName:restoName andRestoImage:restoImage];
            
            }else{
            
                NSLog(@"Map Error : %@", error);
            
                //Prompt that location cannot be geocoded
            
                if ([MoizMenuUtil hasInternetConnection]) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Geocode Error"
                                                                    message:@"Geocode error"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                    [alertView show];
                }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Geocode Error"
                                                                    message:@"No Internet Connection"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                    [alertView show];
                }
            }
        }];
    }
    
}

-(void) plotSelectedRestaurantOnMap:(CLLocation *)location andAddress: (NSString *) address andRestoName: (NSString *)restoName andRestoImage: (NSString *)resotImage{
    
//    MapAnnotation *annotation = [[MapAnnotation alloc] init];
//    annotation.coordinate = location.coordinate;
//    annotation.restoAddress = address;
//    annotation.restoName = restoName;
////    annotation.restoImage
//    [self.gMapView addAnnotation:annotation];
    
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    marker.title = restoName;
    marker.snippet = address;
//    _restoName = restoName;
//    _restoAdd = address;
//    _restoImg = resotImage;
    marker.icon = [UIImage imageNamed:[NSString stringWithFormat:@"%@",resotImage]];
    marker.map = self.gMapView;
    
    NSLog(@"lat %f",location.coordinate.latitude);
    NSLog(@"long %f", location.coordinate.longitude);

}

-(void) openSelectedMapWithLocation: (CLLocation*) location andAddress: (NSString *) address{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    _selectedMapItem = nil;
    
    // Create an MKMapItem to pass to the Maps app
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:location.coordinate
                                                   addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:address];
    

    
    NSLog(@"lat %f",location.coordinate.latitude);
    NSLog(@"long %f", location.coordinate.longitude);
    
    _selectedMapItem = mapItem;
    
    if ([[defaults objectForKey:MAP_KEY] isEqualToString:MAP_APPLE]){
        //APPLE
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
    }else if([[defaults objectForKey:MAP_KEY] isEqualToString:MAP_GOOGLE]){
        //GOOGLE
        NSString *encodedString=[address stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        
        if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:@"comgooglemaps://"]]){
            [[UIApplication sharedApplication] openURL:
             [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%@&center=%f,%f", encodedString, location.coordinate.latitude, location.coordinate.longitude]]];
            
        }else{
            //Cannot open
            NSLog(@"CAnnot open!");
            //ALert for selecting maps
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Google Map installed."
                                                                message:@"The application will use the default Apple Map."
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"OK",nil];
            
            
            alertView.tag = 1;
            [alertView show];
        }
        
        
    }else{
        //ALert for selecting maps
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Default Map is Selected"
                                                            message:@"Please select a default map in Settings"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    
}


#pragma mark - Lazy Load Objects
-(CLGeocoder *) geocoder{
    
    if (!_geocoder){
        _geocoder = [[CLGeocoder alloc] init];
    }
    
    return _geocoder;
}

-(void)loadGMapViewWithFrame:(CGRect)frame {
    
//        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:0
//                                                                longitude:0
//                                                                     zoom:1];
//        _gMapView = [GMSMapView mapWithFrame:frame camera:camera];
    _gMapView.mapType = kGMSTypeNormal;
    
}

- (void)fitMapToShowLocation
{
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        
        CLLocationCoordinate2D myLocation = ((GMSMarker *)_arrPlaceMarks.firstObject).position;
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:myLocation coordinate:myLocation];
        
        for (GMSMarker *marker in _arrPlaceMarks)
            bounds = [bounds includingCoordinate:marker.position];
        
        [self.gMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:20.0f]];
        
    });
    
    
}

//- (UIView *) mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
//{
//    CustomInfoMapInfo *infoMapWindow = [[[NSBundle mainBundle] loadNibNamed:@"CustomInfo" owner:self options:nil]objectAtIndex:0];
//    infoMapWindow.restoNameLabel.text = _restoName;
//    infoMapWindow.restoAddressLabel.text = _restoAdd;
//    infoMapWindow.restoImage.image = [UIImage imageNamed:_restoImg];
////    
////    _restoName = @"";
////    _restoAdd = @"";
////    _restoImg = @"";
//
//    
//    return infoMapWindow;
// }

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
	CGPoint point = [mapView.projection pointForCoordinate:marker.position];
	point.y = point.y - 100;
	GMSCameraUpdate *camera =
    [GMSCameraUpdate setTarget:[mapView.projection coordinateForPoint:point]];
	[mapView animateWithCameraUpdate:camera];
	
	mapView.selectedMarker = marker;
	return YES;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                            longitude:newLocation.coordinate.longitude
                                                                 zoom:17.0];
    [_gMapView animateToCameraPosition:camera];
    //...
}

- (void)goToFilter{
//    FilterViewController *info = [_fetchedResultsController objectAtIndexPath:indexPath];
    FilterViewController* detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
//    [self.navigationController.navigationBarHidden:NO];s
//    FilterViewController *detailViewController = [FilterViewController alloc];
//    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    
}
@end
