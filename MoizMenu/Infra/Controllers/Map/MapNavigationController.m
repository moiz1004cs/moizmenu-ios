//
//  MapNavigationController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/11/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "MapNavigationController.h"

@interface MapNavigationController ()

@end

@implementation MapNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
