//
//  MoizMenuStyle.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/3/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#ifndef MoizMenu_MoizMenuStyle_h
#define MoizMenu_MoizMenuStyle_h

#pragma mark - Font Color


#define GRAY_FONT_COLOR                         [UIColor colorWithRed:161.0/255.0 green:161.0/255.0 blue:161.0/255.0 alpha:1]

#define BlUE_FONT_COLOR                         [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:195.0/255.0 alpha:1]

#define NAVBAR_TINT_COLOR                       [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:195.0/255.0 alpha:1]

//#define BG_COLOR                                [UIColor colorWithRed:0.933 green:0.938 blue:0.938 alpha:1]
#define BG_COLOR                                [UIColor colorWithRed:225.0/255.0 green:235.0/255.0 blue:244.0/255.0 alpha:1]

#define BG_GRAY_COLOR                           [UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:1]

#define BG_IMAGE                                @"FullPageBG.png"

#define DESCRIPTION_FONT_COLOR                  [UIColor colorWithRed:23.0/255.0 green:125.0/255.0 blue:253.0/255.0 alpha:1]

#pragma mark Gray Line Color
#define GRAY_LINE_COLOR                         [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1]

#pragma mark - TableRow Color
#define ROW1_COLOR                              [UIColor colorWithRed:245.0/255.0 green:247.0/255.0 blue:246.0/255.0 alpha:1]
#define ROW2_COLOR                              [UIColor colorWithRed:1 green:1 blue:1 alpha:1]
#define ROW3_COLOR                              [UIColor colorWithRed:245.0/255.0 green:249.0/255.0 blue:252.0/255.0 alpha:1]

#pragma mark Font Color
//#define NYLIFE_GRAY                             [UIColor colorWithRed:0.298 green:0.298 blue:0.298 alpha:1]
#define NYLIFE_GRAY                             [UIColor colorWithRed:67.0/255.0 green:85.0/255.0 blue:92.0/255.0 alpha:1]

//#define NYLIFE_BLUE                             [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:195.0/255.0 alpha:1]
#define NYLIFE_BLUE                             [UIColor colorWithRed:0.0/255.0 green:105.0/255.0 blue:147.0/255.0 alpha:1]

#define NYLIFE_LIGTH_GRAY                       [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1]

#define NYLIFE_LINK_COLOR                       [UIColor colorWithRed:0.0 green:122.0/255.0 blue:194.0/255.0 alpha:1]

#define NYLIFE_BLACK                            [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1]

#define NYLIFE_NAVBAR_BLUE                      [UIColor colorWithRed:13.0/255.0 green:106.0/255.0 blue:145.0/255.0 alpha:1]

#define NYLIFE_SKY_BLUE                         [UIColor colorWithRed:29.0/255.0 green:174.0/255.0 blue:236.0/255.0 alpha:1]

#define NYLIFE_WHITE                            [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1]

#define NYLIFE_GRAY2                            [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1]

#pragma mark Fonts

#define KDASHBOARD_NAVBAR_FONT_WIDGET           [UIFont systemFontOfSize:18]

#pragma mark - Fonts Style
#define kFONT_STYLE_16                          [UIFont fontWithName:@"HelveticaNeue" size:16.0f]

#define kFONT_STYLE_LIGTH_10                    [UIFont fontWithName:@"HelveticaNeue-Light" size:10.0f]
#define kFONT_STYLE_LIGTH_12                    [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0f]
#define kFONT_STYLE_LIGTH_16                    [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0f]
#define kFONT_STYLE_LIGTH_20                    [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0f]
#define kFONT_STYLE_LIGTH_35                    [UIFont fontWithName:@"HelveticaNeue-Light" size:35.0f]
#define kFONT_STYLE_LIGTH_40                    [UIFont fontWithName:@"HelveticaNeue-Light" size:40.0f]

#define kFONT_STYLE_MEDIUM_18                   [UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0f]
#define kFONT_STYLE_MEDIUM_20                   [UIFont fontWithName:@"HelveticaNeue-Medium" size:20.0f]


#define kFONT_STYLE_SEGOEUI_BOLD_20             [UIFont fontWithName:@"seguisb.ttf" size:20.0f]

#define kFONT_STYLE_ALDA_OT_43                  [UIFont fontWithName:@"AldaOT-Regular" size:43.0f]
#define kFONT_STYLE_ALDA_OT_19                  [UIFont fontWithName:@"AldaOT-Regular" size:19.0f]
#define kFONT_STYLE_ALDA_OT_28                  [UIFont fontWithName:@"AldaOT-Regular" size:28.0f]


#define kFONT_STYLE_SEGOEUI_8                   [UIFont fontWithName:@"Segoe UI" size:8.0f]
#define kFONT_STYLE_SEGOEUI_9                   [UIFont fontWithName:@"Segoe UI" size:9.0f]
#define kFONT_STYLE_SEGOEUI_10                  [UIFont fontWithName:@"Segoe UI" size:10.0f]
#define kFONT_STYLE_SEGOEUI_12                  [UIFont fontWithName:@"Segoe UI" size:12.0f]
#define kFONT_STYLE_SEGOEUI_14                  [UIFont fontWithName:@"Segoe UI" size:14.0f]
#define kFONT_STYLE_SEGOEUI_16                  [UIFont fontWithName:@"Segoe UI" size:16.0f]
#define kFONT_STYLE_SEGOEUI_18                  [UIFont fontWithName:@"Segoe UI" size:18.0f]
#define kFONT_STYLE_SEGOEUI_20                  [UIFont fontWithName:@"Segoe UI" size:20.0f]
#define kFONT_STYLE_SEGOEUI_25                  [UIFont fontWithName:@"Segoe UI" size:25.0f]
#define kFONT_STYLE_SEGOEUI_28                  [UIFont fontWithName:@"Segoe UI" size:28.0f
#define kFONT_STYLE_SEGOEUI_30                  [UIFont fontWithName:@"Segoe UI" size:30.0f]
#define kFONT_STYLE_SEGOEUI_35                  [UIFont fontWithName:@"Segoe UI" size:35.0f]
#define kFONT_STYLE_SEGOEUI_40                  [UIFont fontWithName:@"Segoe UI" size:40.0f]

#define kFONT_STYLE_SEGOEUI_BOLD_16             [UIFont fontWithName:@"SegoeUI-Bold" size:16.0f]
#define kFONT_STYLE_SEGOEUI_BOLD_14             [UIFont fontWithName:@"SegoeUI-Bold" size:14.0f]
#define kFONT_STYLE_SEGOEUI_BOLD_12             [UIFont fontWithName:@"SegoeUI-Bold" size:12.0f]

#define kFONT_STYLE_SEGOEUI_SEMI_BOLD_8        [UIFont fontWithName:@"SegoeUI-SemiBold" size:8.0f]
#define kFONT_STYLE_SEGOEUI_SEMI_BOLD_10        [UIFont fontWithName:@"SegoeUI-SemiBold" size:10.0f]
#define kFONT_STYLE_SEGOEUI_SEMI_BOLD_11        [UIFont fontWithName:@"SegoeUI-SemiBold" size:11.0f]
#define kFONT_STYLE_SEGOEUI_SEMI_BOLD_12        [UIFont fontWithName:@"SegoeUI-SemiBold" size:12.0f]
#define kFONT_STYLE_SEGOEUI_SEMI_BOLD_14        [UIFont fontWithName:@"SegoeUI-SemiBold" size:14.0f]



#pragma mark - Button_Image

#define IMAGE_SEARCH                            @"Search_NavIcon.png"
#define IMAGE_LOGOUT                            @"LogOut_NavIcon.png"


#define MAP_ORIGIN                              @"Map_Origin"
#define RESTAURANT_DETAIL_VIEW_ORIGIN           @"Restaurant_DetailView"
#define RESTAURANT_MENU_ORIGIN                  @"Restaurant_Menu"
#define RESRAURANT_MAIN                         @"Restaurant_Main"
#define FILTER_ORIGIN                           @"Filter_View"
#define SPECIFIC_RESTO                          @"Specific_Resto"


#endif
