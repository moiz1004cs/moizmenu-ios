//
//  Restaurant.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/21/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Menu;

@interface Restaurant : NSManagedObject

@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * cuisine;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * mainRestoId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * priceRangeEnd;
@property (nonatomic, retain) NSNumber * priceRangeStart;
@property (nonatomic, retain) NSNumber * rate;
@property (nonatomic, retain) NSString * restoId;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * zipcode;
@property (nonatomic, retain) NSString * originType;
@property (nonatomic, retain) NSString * imageUrl;
@property (nonatomic, retain) Menu *menu;

@end
