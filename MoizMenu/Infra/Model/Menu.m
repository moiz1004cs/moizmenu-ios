//
//  Menu.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/21/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "Menu.h"
#import "MenuItem.h"
#import "Restaurant.h"


@implementation Menu

@dynamic menuId;
@dynamic restaurantId;
@dynamic menuItem;
@dynamic restaurant;

@end
