//
//  AppDelegate.h
//  ;
//
//  Created by Abdul Esmail on 1/30/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuItem.h"
#import "Menu.h"
#import <GoogleMaps/GoogleMaps.h>
#import "TaBarViewController.h"
#import "SFRestAPI.h"
#import "User.h"
#import "ChatterRequestHelper.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, SFRestDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator ;
@property (nonatomic, strong) TaBarViewController *rootVC;
@property (strong, nonatomic) NSArray *restaurantArray;
@property (strong, nonatomic) ChatterRequestHelper *chatterRequestHelper;

-(NSArray*)getAllRestaurant;
-(void)loadDataFromPListToCoreData;
-(Menu *)getMenuById:(Menu *)restoId;
-(NSArray *)getAllCuisine;
-(NSArray *)getAllPriceRangeEnd;
-(NSArray *)getAllPriceRangeStart;
-(NSArray *)getAllBranchesByRestoId:(NSString *) restoId;
-(Restaurant *)getRestaurantByName:(NSString *)restoName;
-(Restaurant *)getRestaurantById:(NSString *)restoId;
-(NSArray *)getRestaurantByFilter:(NSString *) restoName cuisineName:(NSString *)cuisineName priceRangeStart:(NSString *)priceRangeStart priceRangeEnd:(NSString *)priceRangeEnd;
-(NSString *)getMainBranchImage:(NSString *)restoId;
-(User *)getUserProfile;
-(void)sfQueryRestaurant;
- (void) massDeleteCoreData;



//-(NSArray*)loadDataFromPListToCoreData;


@end
