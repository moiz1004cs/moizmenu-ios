//
//  MapViewController.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/7/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "FilterViewController.h"

@interface MapViewController : UIViewController<MKMapViewDelegate, GMSMapViewDelegate, CLLocationManagerDelegate, FilterViewDelegate>

@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, retain) GMSMapView *gMapView;
@property (weak, nonatomic) IBOutlet UIView *mainView;

//@property (nonatomic, strong) IBOutlet UIBarButton *filterButton;
//@property (weak, nonatomic) IBOutlet UIBarButton *filterButton;
@property (nonatomic, strong) IBOutlet UITextField *nameField;
@property (nonatomic, strong) IBOutlet UITextField *priceRangeField;
@property (nonatomic, strong) IBOutlet UITextField *cuisineField;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;

- (void)goToFilter;


@end
