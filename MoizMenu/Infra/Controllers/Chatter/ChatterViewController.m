//
//  ChatterViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/27/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "ChatterViewController.h"
#import "ChatterObject.h"
#import "AppDelegate.h"
#import "ChtterTableCell.h"
#import "SFAccountManager.h"
#import "SFAuthenticationManager.h"
#import "SFPushNotificationManager.h"
#import "MoizMenuUtil.h"
#import "DownloadImage.h"
#import "AddChatterViewController.h"
#import "GTMNSString+HTML.h"
#import "NSString+HTML.h"


//#import "GTMNSString+HTML.h"
//#import "NSString+HTML.h"


#define APP_DELEGATE                    ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface ChatterViewController ()

@property (nonatomic, strong) NSMutableArray *feedResult;
@property (nonatomic, strong) NSMutableArray *chatterData;
@property (nonatomic, strong) NSMutableArray *meChatterData;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *data;
@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) DownloadImage *downloadImage;
@property (nonatomic, strong) AddChatterViewController *addChatterView;

@end

@implementation ChatterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadChattersAtMe)
                                                 name:@"Finished_loading_chatter_feed"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadChatters)
                                                 name:@"Finished_loading_chatter_feed"
                                               object:nil];
    

    [self.allChatterButton addTarget:self action:@selector(loadChatters) forControlEvents:UIControlEventTouchUpInside];
    [self.atMeChatterButton addTarget:self action:@selector(loadChattersAtMe) forControlEvents:UIControlEventTouchUpInside];
	// Do any additional setup after loading the view.
    [APP_DELEGATE.chatterRequestHelper getChatterItems];
    [APP_DELEGATE.chatterRequestHelper getMeItems];
    
//    [self loadChatters];
    

}
- (void) loadChatters{
    _chatterData = [APP_DELEGATE.chatterRequestHelper returnChatterItems];
    NSLog(@"Count of result: %i", _chatterData.count);
    
    _feedResult = [[NSMutableArray alloc]init];
    for(int i=0; i < _chatterData.count; i++){
        ChatterObject *curPost = [_chatterData objectAtIndex:i];
        NSLog(@"Current Post: %@",curPost.user.fullName);
        [_feedResult addObject:curPost];
    }

    [_tableView reloadData];

}

- (void) loadChattersAtMe{
    _meChatterData = [APP_DELEGATE.chatterRequestHelper returnMeItems];
    NSLog(@"Count of result: %i", _meChatterData.count);
    
    _feedResult = [[NSMutableArray alloc]init];
    for(int i=0; i < _meChatterData.count; i++){
        ChatterObject *curPost = [_meChatterData objectAtIndex:i];
        NSLog(@"Current Post: %@",curPost.user.fullName);
        [_feedResult addObject:curPost];
    }
    
    [_tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_feedResult count];
//    return  1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"ChatterTableCell";
    ChtterTableCell *cell = (ChtterTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[ChtterTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSArray *topLevelObjects = [[NSBundle mainBundle]loadNibNamed:@"ChatterTableCell" owner:nil options:nil];
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[ChtterTableCell class]])
        {
            cell = (ChtterTableCell *)currentObject;
            break;
        }
    }
    if(_feedResult && indexPath.row <_feedResult.count){
        ChatterObject *curObj = [_feedResult objectAtIndex:indexPath.row];
        [cell.nameLabel setText:curObj.user.fullName];
//        _imageView = cell.imageView;
//        [self.downloadImage downloadSmallProfileImageAsyncFromURL:curObj.user.smallProfileImageURL imageView:cell.imageView];
        cell.imageView.image = [UIImage imageWithData:[self.downloadImage getTheImageData]];
        cell.likeLabel.text =[NSString stringWithFormat:@"%@ likes", curObj.numLikes];
        cell.commetLabel.text = [NSString stringWithFormat:@"%@ commnets", curObj.numComments];
        cell.messageLabel.text = [NSString stringWithFormat:@"%@",curObj.postContent];
//        NSString * strPost =[curObj.postContent gtm_stringByUnescapingFromHTML];
//        [cell.messageLabel setText:[curObj.postContent gtm_stringByUnescapingFromHTML]];
        cell.imageView.image = [UIImage imageWithData:[MoizMenuUtil getImageFromURLLink:curObj.user.smallProfileImageURL]];
//        [self downloadSmallProfileImageAsyncFromURL:curObj.user.smallProfileImageURL imageView:cell.imageView];
    }
//    [cell.nameLabel setText:<#(NSString *)#>
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 82;
    
}

-(void)downloadSmallProfileImageAsyncFromURL:(NSURL*)url imageView:(UIImageView *)imageView{
    _imageView = imageView;
    NSMutableURLRequest *req = [MoizMenuUtil getAuthenticatedRequestWithURL:url];
    _connection = [NSURLConnection connectionWithRequest:req delegate:self];
    [_connection start];
}

+ (NSMutableURLRequest *)getAuthenticatedRequestWithURL:(NSURL *)url
{
    SFOAuthCredentials *creds = [[SFRestAPI sharedInstance] coordinator].credentials;
    if (creds != nil) {
        NSString *value = [NSString stringWithFormat:@"OAuth %@", creds.accessToken];
        NSDictionary *headers = [NSDictionary dictionaryWithObject:value forKey:@"Authorization"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        for (NSString *header in headers)
        {
            NSString *value = [headers objectForKey:header];
            [request addValue:value forHTTPHeaderField:header];
        }
        [request setHTTPMethod:@"GET"];
        return request;
    } else {
        return nil;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	_data = [NSMutableData dataWithCapacity:1024];
	[_data setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)inData {
	[_data appendData:inData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *fullPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[MoizMenuUtil getUniqueId]]];
    
    [_data writeToFile:fullPath atomically:YES];
    
	self.imageView.image = [UIImage imageWithData:_data];
    //self.profileImageFullPath = fullPath;
	_data = nil;
    self.imageView = nil;
	connection = nil;
    
    //DLog(@"File Path when saved: %@",self.profileImageFullPath);
}
-(IBAction)goToAddChatter:(id)sender{
    
//    float height = [MoizMenuUtil windowHeight] - (64*2);
    _addChatterView = [[AddChatterViewController alloc] initWithNibName:@"AddChatterViewController" bundle:nil];
    [self.navigationController pushViewController:_addChatterView animated:YES];
    //    _addChatterView.selectedRestaurant = self.selectedRestaurant;
//    _addChatterView.origin = RESTAURANT_DETAIL_VIEW_ORIGIN;
    //    [self. rightViewPushViewControllerOverCenterController:_storeMenuViewController];
    
//    [self.view addSubview:_addChatterView.view];
//    
//    
//    [_addChatterView.view setFrame:CGRectMake(320,0, 320, height)];
//    
//    NSLog(@"rect %@",NSStringFromCGRect(_addChatterView.view.frame));
//    
//    [UIView animateWithDuration:0.3 animations:^{
//        [_addChatterView.view setFrame:CGRectMake(0,0, 320, height)];
//    } completion:^(BOOL isDone){
//        
//    }];
    
}
@end
