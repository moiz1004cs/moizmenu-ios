//
//  GMSLocation.m
//  NYLife
//
//  Created by Matthew Casey on 05/09/2013.
//  Copyright (c) 2013 CloudSherpas. All rights reserved.
//

#import "GMSLocation.h"

@implementation GMSLocation
+(GMSLocation *)locationWithAddress:(NSString *)aAddress {
    GMSLocation *ret = [[GMSLocation alloc] init];
    ret.useAddress = YES;
    ret.address = aAddress;
    return ret;
}
+(GMSLocation *)locationWithLatitude:(float)lat longitude:(float)lng {
    GMSLocation *ret = [[GMSLocation alloc] init];
    ret.useAddress = NO;
    LatLng latLng;
    latLng.latitude = lat;
    latLng.longitude = lng;
    ret.latLng = latLng;
    return ret;
}
-(NSString *)stringFromLocation {
    if (self.useAddress) {
        return self.address;
    } else {
        return [NSString stringWithFormat:@"%f,%f", self.latLng.latitude, self.latLng.longitude];
    }
}
@end
