//
//  FilterViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/11/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "FilterViewController.h"
#import "Restaurant.h"
#import "AppDelegate.h"
#import "AppleMapViewController.h"
#import "MoizMenuStyle.h"

#define APP_DELEGATE                    ((AppDelegate *)[[UIApplication sharedApplication] delegate])


//#define priceRangeStartPicker 1
//#define priceRangeEndPicker 2
//#define cuisinePicker 3

//#define f_cuisine  1
//#define f_startRange  2
//#define f_endRange  3

typedef enum{
    f_cuisine = 100,
    f_startRange = 200,
    f_endRange = 300
}filterFields;

@interface FilterViewController ()

@property (nonatomic, strong) AppleMapViewController *appleView;

@property (strong, nonatomic) NSMutableArray *fetchedRecordsArray;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSMutableArray *finaArray;

@property (nonatomic) BOOL priceRangeStartPicker;
@property (nonatomic) BOOL priceRangeEndPicker;
@property (nonatomic) BOOL cuisinePicker;

@end
@implementation FilterViewController

@synthesize priceRangeArrayStart;
@synthesize priceRangeArrayEnd;
@synthesize cuisineArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    priceRangeArrayStart = [[NSMutableArray alloc] initWithCapacity:0];
    priceRangeArrayEnd = [[NSMutableArray alloc] initWithCapacity:0];
    cuisineArray = [[NSMutableArray alloc]initWithCapacity:0];
    // Do any additional setup after loading the view from its nib.
//    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
//    self.managedObjectContext = appDelegate.managedObjectContext;
    self.cuisineArray = [[APP_DELEGATE getAllCuisine]mutableCopy];
    
//
    
//    _finaArray = [[NSMutableArray alloc]initWithCapacity:0];
    [self.navigationController setNavigationBarHidden:YES];
    
    [priceRangeArrayStart addObject:@"0"];
    [priceRangeArrayStart addObject:@"200"];
    [priceRangeArrayStart addObject:@"400"];
    [priceRangeArrayStart addObject:@"600"];
    [priceRangeArrayStart addObject:@"800"];
    
    [priceRangeArrayEnd addObject:@"200"];
    [priceRangeArrayEnd addObject:@"400"];
    [priceRangeArrayEnd addObject:@"600"];
    [priceRangeArrayEnd addObject:@"800"];
    [priceRangeArrayEnd addObject:@"1000"];
    
//    self.priceRangeArrayStart =[[appDelegate getAllPriceRangeStart]mutableCopy];
//    self.priceRangeArrayEnd = [[appDelegate getAllPriceRangeEnd]mutableCopy];
    
    _cusineName.tag = f_cuisine;
    _priceRangeStart.tag = f_startRange;
    _priceRangeEnd.tag = f_endRange;
    
    
//    _cuisinePickerView.tag = f_cuisine;
    
    _cusineName.inputView = _cuisinePickerView;
    _priceRangeStart.inputView = _cuisinePickerView;
    _priceRangeEnd.inputView = _cuisinePickerView;
    
    _priceRangeStart.delegate = self;
    _priceRangeEnd.delegate = self;
    _cusineName.delegate = self;
    
    
    [_cuisinePickerView setDelegate:self];
    [_cuisinePickerView setDataSource:self];
    [self.submitFilterButton addTarget:self action:@selector(goToMapWithData) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton addTarget:self action:@selector(goToMap) forControlEvents:UIControlEventTouchUpInside];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)picker numberOfRowsInComponent:(NSInteger)component
{
    
    if(picker.tag == f_cuisine){
        return [cuisineArray count];
    }else if(picker.tag == f_startRange){
        return [priceRangeArrayStart count];
    }else if(picker.tag == f_endRange){
        return [priceRangeArrayEnd count];
    }else{
        return 1;
    }

}

-(NSString *) pickerView:(UIPickerView *)picker titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if(picker.tag == f_cuisine){
        return [cuisineArray objectAtIndex:row];
    }else if(picker.tag == f_startRange){
        return [priceRangeArrayStart objectAtIndex:row];
    }else if(picker.tag == f_endRange){
        return [priceRangeArrayEnd objectAtIndex:row];
    }else{
        return nil;
    }
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

//    [textField setInputAccessoryView:_cuisinePickerView];
//    NSLog(@"_cuisinePickerView.tag  = %d", textField.tag );
    
    _cuisinePickerView.tag = textField.tag;
    NSLog(@"_cuisinePickerView.tag  = %d", _cuisinePickerView.tag);
    [_cuisinePickerView reloadAllComponents];
    
    return YES;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component
{
    if(pickerView.tag == f_cuisine){
        _cusineName.text = cuisineArray[row];
    }else if(pickerView.tag == f_startRange){
        NSLog(@"Selected: %@", priceRangeArrayStart[row]);
        _priceRangeStart.text =priceRangeArrayStart[row];
//         _priceRangeStart.text = @"200";
    }else if(pickerView.tag == f_endRange){
        _priceRangeEnd.text = priceRangeArrayEnd[row];
    }
}


-(void)goToMapWithData{
    
    if(_priceRangeStart.text <= _priceRangeEnd.text){
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AppleMapViewController * yourView = (AppleMapViewController *)[storyboard instantiateViewControllerWithIdentifier:@"AppleMapViewController"];
        
        yourView.origin = FILTER_ORIGIN;
        yourView.filterRestoName = _restoName.text;
        yourView.filterCuisine = _cusineName.text;
        yourView.filterPriceStarRange = _priceRangeStart.text;
        yourView.filterPRiceEndRange = _priceRangeEnd.text;
//        [self.navigationController popViewControllerAnimated:YES];
        [self.navigationController pushViewController:yourView animated:NO];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Filter Error"
                                                           message:@"Price range did not coinside"
                                                          delegate:nil
                                                 cancelButtonTitle:@"Ok"
                                                 otherButtonTitles:nil];
        [alertView show];

    }
    
   
    

    
}
-(void)goToMap{
    
//    _appleView = [[AppleMapViewController alloc] initWithNibName:@"AppleMapViewController" bundle:nil];
//    //        _storeMenu.selectedRestaurant = resto;
//    [self.navigationController pushViewController:_appleView animated:YES];
    [self.navigationController popViewControllerAnimated:YES];

}

@end
