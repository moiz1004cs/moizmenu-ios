//
//  ChatterUser.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/27/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "ChatterUser.h"
#import "SFOAuthCoordinator.h"
#import "SFRestAPI.h"

@interface ChatterUser () <NSURLConnectionDelegate>
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *data;
@end

@implementation ChatterUser

-(id)initWithUserDict:(NSDictionary*)userDict {
    ChatterUser *user = [[ChatterUser alloc] init];
    user.userData = userDict;
    user.userID = [userDict objectForKey:@"id"];
    user.fullName = [NSString stringWithFormat:@"%@ %@",[userDict objectForKey:@"firstName"],[userDict objectForKey:@"lastName"]];
    user.companyName = [userDict objectForKey:@"companyName"];
    NSDictionary *photo = [userDict objectForKey:@"photo"];
    user.smallProfileImageURL = [NSURL URLWithString:[photo objectForKey:@"smallPhotoUrl"]];
    // [user downloadSmallProfileImageAsyncFromURL];
    
    return user;
}


@end
