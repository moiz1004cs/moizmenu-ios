//
//  RestaurantTableCell.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/4/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantTableCell : UITableViewCell

//@property (weak, nonatomic) IBOutlet UILabel *restoNameLabel;
//@property (weak, nonatomic) IBOutlet UILabel *cuisineLabel;
//@property (weak, nonatomic) IBOutlet UILabel *priceRangeLabel;
@property (nonatomic, strong) IBOutlet UILabel *restoNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *cuisineLabel;
@property (nonatomic, strong) IBOutlet UILabel *priceRangeLabel;
@property (nonatomic, strong) IBOutlet UIImageView *restoImageView;
@end
