//
//  AppleMapViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/11/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "AppleMapViewController.h"
#import "AppDelegate.h"
#import "Restaurant.h"
#import "MoizMenuStyle.h"
#import "MoizMenuUtil.h"
#import "AFNetworking.h"
#import "MapAnnotation.h"
#import "RestaurantDetailViewController.h"
#import "StoreMenuViewController.h"

#define APP_DELEGATE                    ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define MAP_KEY     @"AIzaSyCQf3H1rRiO6066DibQll96yUgKh6hvftM"
#define MAP_APPLE   @"Apple Map"
#define MAP_GOOGLE  @"Google Map"
#define ZOOM_LEVEL  15


@interface AppleMapViewController (){
    
    CLLocationManager *_locationManager;
    NSMutableArray *_arrayOfNearbyRestaurant;
    
    CLLocationCoordinate2D mapLocation;
}
@property (nonatomic, strong) NSMutableArray *arrPlaceMarks;
@property (nonatomic, strong) NSMutableArray *arrayDict;
@property (nonatomic, strong) NSArray* fetchedRecordsArray;
@property (nonatomic, strong) NSMutableDictionary * addressDictionary;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) CLGeocoder *geocoder;
@property (nonatomic, strong) MKMapItem *selectedMapItem;

@property (nonatomic, strong) NSString *restoName;
@property (nonatomic, strong) NSString *restoAdd;
@property (nonatomic, strong) NSString *restoImg;
@property CLLocationCoordinate2D coords;

@property (nonatomic, strong) NSMutableArray *latitudes;
@property (nonatomic, strong) NSMutableArray *longitudes;

@property (nonatomic, copy) NSString *restaurantImageName;
@property (nonatomic) BOOL *isImageUrl;

@property (nonatomic, strong) UIPopoverController *popover;

@property (nonatomic, strong) FilterViewController* filter;
@property (nonatomic, strong) RestaurantDetailViewController *restaurantView;
@property (nonatomic, strong) StoreMenuViewController *storeMenu;

//@property (nonatomic) Boolean 

//@property (nonatomic, strong) R/
@end

@implementation AppleMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.navigationController setNavigationBarHidden:YES];
    }
    return self;
}

-(void)viewDidUnload{
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    
    self.mapView.delegate = self;
//    CLLocation *location = [_locationManager location];
    
    _addressDictionary = [[NSMutableDictionary alloc]init];
    
//    mapLocation = _locationManager.location.coordinate;
    
    [self.view insertSubview:_filterButton atIndex:1];
    
    [self.view insertSubview:self.mapView atIndex:0];
    
    [self.mapView setMapType:MKMapTypeStandard];
    [self.mapView setZoomEnabled:YES];
    [self.mapView setScrollEnabled:YES];
    
    [self.mapView setDelegate:self];
    

    if([self.origin isEqualToString:SPECIFIC_RESTO]){
        
       
        [self.view insertSubview:_nameField atIndex:0];
        [self.view insertSubview:_cuisineField atIndex:0];
        [self.view insertSubview:_priceRangeField atIndex:0];
        
        [self geocodeLocationWithResto:self.restaurant];
        
    }else if([self.origin isEqualToString:FILTER_ORIGIN]){
        
        [_locationManager startUpdatingLocation];
        _locationManager.delegate = self;
        
//        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.mapView.userLocation.location.coordinate, 30, 30);
//        
//        [self.mapView setRegion:region animated:YES];
        NSLog(@"Filtering");
        _latitudes = [[NSMutableArray alloc] init];
        _longitudes = [[NSMutableArray alloc] init];
        [self.view insertSubview:_nameField atIndex:1];
        [self.view insertSubview:_cuisineField atIndex:1];
        [self.view insertSubview:_priceRangeField atIndex:1];
        
        if([self.filterRestoName isEqualToString:@""]){
            self.nameField.hidden = YES;
//            self.filterRestoName = @"";
        }
        
        if([self.filterCuisine isEqualToString:@""]){
            self.cuisineField.hidden = YES;
//            self.filterCuisine = @"";
        }
        
        if([self.filterPriceStarRange isEqualToString:@""] && [self.filterPRiceEndRange isEqualToString:@""]){
            self.priceRangeField.hidden = YES;
        }
        
        [self.nameField setTitle:self.filterRestoName forState:UIControlStateNormal];
        [self.cuisineField setTitle:self.filterCuisine forState:UIControlStateNormal];
        [self.priceRangeField setTitle:[NSString stringWithFormat:@"%@-%@", self.filterPriceStarRange,self.filterPRiceEndRange] forState:UIControlStateNormal];
        
        self.fetchedRecordsArray = [[APP_DELEGATE getRestaurantByFilter:self.filterRestoName cuisineName:self.filterCuisine priceRangeStart:self.filterPriceStarRange priceRangeEnd:self.filterPRiceEndRange]mutableCopy];
        
        for(Restaurant * resto in self.fetchedRecordsArray){
            NSString *strAddress = [NSString stringWithFormat:@"%@, %@, %@, %@", resto.street, resto.city, resto.state, resto.zipcode];
            [_addressDictionary setValue:strAddress forKey:@"address"];
            [_addressDictionary setValue:resto.name forKey:@"restoName"];
            
            if([resto.type isEqualToString:@"Branch"]){
//                AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
                [_addressDictionary setValue:[APP_DELEGATE getMainBranchImage:resto.mainRestoId] forKey:@"image"];
            }else{
                [_addressDictionary setValue:resto.image forKey:@"image"];
            }
            
            [self geocodeLocationWithAddress:_addressDictionary];
            
            if([_latitudes count] > 0){
                [self updateRegion];
                
            }
        }
        
    }else{
        NSLog(@"Why I'm here");
        
         [_locationManager startUpdatingLocation];
        _locationManager.delegate = self;
//        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.mapView.userLocation.location.coordinate, 30, 30);
//        
//        [self.mapView setRegion:region animated:YES];
        
        _latitudes = [[NSMutableArray alloc] init];
        _longitudes = [[NSMutableArray alloc] init];
        
        [self.view insertSubview:_nameField atIndex:0];
        [self.view insertSubview:_cuisineField atIndex:0];
        [self.view insertSubview:_priceRangeField atIndex:0];
        
        [self.nameField setTitle:@"" forState:UIControlStateNormal];
        [self.cuisineField setTitle:@"" forState:UIControlStateNormal];
        [self.priceRangeField setTitle:@"" forState:UIControlStateNormal];
  
        self.mapView.showsUserLocation = YES;
        
        //Define map view region
        
        
        self.fetchedRecordsArray = [[APP_DELEGATE getAllRestaurant]mutableCopy];

        
        for(Restaurant * resto in self.fetchedRecordsArray){
            NSString *strAddress = [NSString stringWithFormat:@"%@, %@, %@, %@", resto.street, resto.city, resto.state, resto.zipcode];
            [_addressDictionary setValue:strAddress forKey:@"address"];
            [_addressDictionary setValue:resto.name forKey:@"restoName"];
            [_addressDictionary setValue:resto.image forKey:@"image"];
            
            if([resto.type isEqualToString:@"Branch"]){
                
                if([resto.originType isEqualToString:@"Salesforce"]){
                    [_addressDictionary setValue:[APP_DELEGATE getMainBranchImage:resto.mainRestoId] forKey:@"image"];
                }else{
                    [_addressDictionary setValue:[APP_DELEGATE getMainBranchImage:resto.mainRestoId] forKey:@"image"];
                }
                
            }else{
                if([resto.originType isEqualToString:@"Salesforce"]){
                    
                    [_addressDictionary setValue:[MoizMenuUtil getImageFromURL:resto.imageUrl] forKey:@"image"];
                }else{
                    [_addressDictionary setValue:[UIImage imageNamed:resto.image] forKey:@"image"];
                }
                
            }

            
            [self geocodeLocationWithAddress:_addressDictionary];
            
        }

    }
    
    [self.navigationController setNavigationBarHidden:YES];
    [self.filterButton addTarget:self action:@selector(goToFilter) forControlEvents:UIControlEventTouchUpInside];
}


-(void) updateRegion{
    [_latitudes sortUsingSelector:@selector(compare:)];
    [_longitudes sortUsingSelector:@selector(compare:)];
    [_latitudes sortUsingSelector:@selector(compare:)];
    [_longitudes sortUsingSelector:@selector(compare:)];
    
    double smallestLat = [_latitudes[0] doubleValue];
    double smallestLng = [_longitudes[0] doubleValue];
    double biggestLat = [[_latitudes lastObject] doubleValue];
    double biggestLng = [[_longitudes lastObject] doubleValue];
    
    CLLocationCoordinate2D annotationsCenter =
    CLLocationCoordinate2DMake((biggestLat + smallestLat) / 2,
                               (biggestLng + smallestLng) / 2);
    
    MKCoordinateSpan annotationsSpan =
    MKCoordinateSpanMake((biggestLat - smallestLat),
                         (biggestLng - smallestLng));
    
    MKCoordinateRegion region =
    MKCoordinateRegionMake(annotationsCenter, annotationsSpan);
    [self.mapView setRegion:region];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//FUCTIONS FROM NYLIFE
- (void)geocodeLocateWithAddressOrCustom:(NSString *) customAddress isCurrent:(BOOL) isCurrent{
    
    
    if (isCurrent){
        //Method to zoom map, update global declaration of location
        mapLocation = _locationManager.location.coordinate;
        
        //  DLog(@"***MAP CURRENT_LOCATION_LAT: %f", mapLocation.latitude);
        //  DLog(@"***MAP CURRENT_LOCATION_LONG: %f", mapLocation.longitude);
//        self.mapView.camera = [GMSCameraPosition cameraWithTarget:mapLocation zoom:ZOOM_LEVEL];
    }else{
        //geocode, zoom to map, update global declaration of location
        
        [self.geocoder geocodeAddressString:customAddress completionHandler:^(NSArray* placemarks, NSError* error){
            
            if ([placemarks count] > 0){
                CLPlacemark *placemark = [placemarks lastObject];
                //zoom to map. update global declaration
                mapLocation = placemark.location.coordinate;
                
                // DLog(@"***MAP CUSTOM_LOCATION_LAT: %f", mapLocation.latitude);
                // DLog(@"***MAP CUSTOM_LOCATION_LONG: %f", mapLocation.longitude);
//                self.mapView.camera = [GMSCameraPosition cameraWithTarget:mapLocation zoom:ZOOM_LEVEL];
                
                
            }else{
                //Prompt that location cannot be geocoded
                
                NSLog(@"Map Error : %@", error);
                
                if ([MoizMenuUtil hasInternetConnection]) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Geocode Error"
                                                                        message:@"Error on GeOcode"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"Ok"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }else{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Geocode Error"
                                                                        message:@"No internet connection"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"Ok"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
                
                
            }
            
        }];
    }
    
}

-(void) geocodeLocationWithResto:(Restaurant *) resto{
    
    NSString *address = [NSString stringWithFormat:@"%@, %@, %@, %@", resto.street, resto.city, resto.state, resto.zipcode];
    NSString *restoName = resto.name;
    
    NSString *restoImage;
    
    if([resto.type isEqualToString:@"Branch"]){
        
        if([resto.originType isEqualToString:@"Salesforce"]){
            _isImageUrl = YES;
            restoImage = [APP_DELEGATE getMainBranchImage:resto.imageUrl];
        }else{
            _isImageUrl = NO;
            restoImage = [APP_DELEGATE getMainBranchImage:resto.mainRestoId];
        }
    }else{

        if([resto.originType isEqualToString:@"Salesforce"]){
            _isImageUrl = YES;
            restoImage = resto.imageUrl;
        }else{
            _isImageUrl = NO;
            restoImage = resto.image;
        }

    }

    
    if(address){
        self.geocoder = nil;
        [self.geocoder geocodeAddressString:address completionHandler:^(NSArray* placemarks, NSError* error){
            
            if ([placemarks count] > 0){
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                
                [self plotSelectedRestaurantOnMap:placemark.location andAddress:address andRestoName:restoName andRestoImage:restoImage isURL:_isImageUrl isSingle:YES];
                
                NSLog(@"Resto Name: %@", restoName);
                NSLog(@"Address: %@", address);

                
            }else{
                
                NSLog(@"Map Error : %@", error);
                
                //Prompt that location cannot be geocoded
                
                if ([MoizMenuUtil hasInternetConnection]) {
                    
                    NSLog(@"ERROR: Cannot locate %@",restoName);
                }else{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Geocode Error"
                                                                        message:@"No Internet Connection"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"Ok"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
            }
        }];
    }
    
}


-(void) geocodeLocationWithAddress:(NSDictionary *) dict{
    
    
    NSString *address = [dict objectForKey:@"address"];
    NSString *restoName = [dict objectForKey:@"restoName"];
    NSString *restoImage = [dict objectForKey:@"image"];

    
    
    if(address){
        self.geocoder = nil;
        [self.geocoder geocodeAddressString:address completionHandler:^(NSArray* placemarks, NSError* error){
            
            if ([placemarks count] > 0){
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                
                NSLog(@"Resto Name: %@", restoName);
                NSLog(@"Address: %@", address);
                
                [self plotSelectedRestaurantOnMap:placemark.location andAddress:address andRestoName:restoName andRestoImage:restoImage isURL:_isImageUrl isSingle:NO];
                
            }else{
                
                NSLog(@"Map Error : %@", error);
                
                if ([MoizMenuUtil hasInternetConnection]) {
                    
                    NSLog(@"ERROR: Cannot locate %@",restoName);
                }else{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Geocode Error"
                                                                        message:@"No Internet Connection"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"Ok"
                                                              otherButtonTitles:nil];
                    [alertView show];
                }
            }
        }];
    }
    
}

-(void) plotSelectedRestaurantOnMap:(CLLocation *)location andAddress: (NSString *) address andRestoName: (NSString *)restoName andRestoImage: (NSString *)resotImage isURL:(BOOL *)isURL isSingle:(Boolean *)isSingle{

    NSLog(@"lat %f",location.coordinate.latitude);
    NSLog(@"long %f", location.coordinate.longitude);
    [_latitudes addObject:[NSNumber numberWithDouble:location.coordinate.latitude ]];
    [_longitudes addObject:[NSNumber numberWithDouble:location.coordinate.longitude]];
    
    MapAnnotation *annotation = [[MapAnnotation alloc] init];
    if(isSingle){
        MKCoordinateSpan span;
        span.latitudeDelta=.003;
        span.longitudeDelta=.003;
        
        MKCoordinateRegion region;
        region.span=span;
        region.center=CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        [self.mapView setRegion:region animated:YES];
    }
    
    
    annotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    annotation.title = restoName;
    annotation.subtitle = address;
    
    _restaurantImageName = resotImage;
    [_mapView addAnnotation:annotation];
    
}

-(void) openSelectedMapWithLocation: (CLLocation*) location andAddress: (NSString *) address{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    _selectedMapItem = nil;
    
    // Create an MKMapItem to pass to the Maps app
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:location.coordinate
                                                   addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:address];
    
    
    
    NSLog(@"lat %f",location.coordinate.latitude);
    NSLog(@"long %f", location.coordinate.longitude);
    
    _selectedMapItem = mapItem;
    
    if ([[defaults objectForKey:MAP_KEY] isEqualToString:MAP_APPLE]){
        //APPLE
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
    }else if([[defaults objectForKey:MAP_KEY] isEqualToString:MAP_GOOGLE]){
        //GOOGLE
        NSString *encodedString=[address stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        
        if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:@"comgooglemaps://"]]){
            [[UIApplication sharedApplication] openURL:
             [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%@&center=%f,%f", encodedString, location.coordinate.latitude, location.coordinate.longitude]]];
            
        }else{
            //Cannot open
            NSLog(@"CAnnot open!");
            //ALert for selecting maps
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Google Map installed."
                                                                message:@"The application will use the default Apple Map."
                                                               delegate:self
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"OK",nil];
            
            
            alertView.tag = 1;
            [alertView show];
        }
        
        
    }else{
        //ALert for selecting maps
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Default Map is Selected"
                                                            message:@"Please select a default map in Settings"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    
}


#pragma mark - Lazy Load Objects
-(CLGeocoder *) geocoder{
    
    if (!_geocoder){
        _geocoder = [[CLGeocoder alloc] init];
    }
    
    return _geocoder;
}

-(void)loadGMapViewWithFrame:(CGRect)frame {
    
    _mapView.mapType = kGMSTypeNormal;
    
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
	CGPoint point = [mapView.projection pointForCoordinate:marker.position];
	point.y = point.y - 100;
	GMSCameraUpdate *camera =
    [GMSCameraUpdate setTarget:[mapView.projection coordinateForPoint:point]];
	[mapView animateWithCameraUpdate:camera];
	
	mapView.selectedMarker = marker;
	return YES;
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    _mapView.centerCoordinate =userLocation.location.coordinate;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    MKCoordinateSpan span;
    span.latitudeDelta=.05;
    span.longitudeDelta=.05;
    
    MKCoordinateRegion region;
    region.span=span;
    region.center=CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    [self.mapView setRegion:region animated:YES];
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *pinView = nil;
    if(annotation != self.mapView.userLocation)
    {
        static NSString *defaultPinID = @"com.invasivecode.pin";
        pinView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        
        pinView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeInfoDark];
        pinView.canShowCallout = YES;
        [pinView setEnabled:YES];
        
        pinView.image = [UIImage imageNamed:@"locate.png"];
        
//        if(_isImageUrl){
//            pinView.image = [UIImage imageWithData:[MoizMenuUtil getImageFromURL:_restaurantImageName]];
//        }else{
//            pinView.image = [UIImage imageNamed:_restaurantImageName];
//        }
//
        [pinView setEnabled:YES];
        [pinView setCanShowCallout:YES];
    }
    else {
        [self.mapView.userLocation setTitle:@"I am here"];
    }
    return pinView;
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    
    NSLog(@"Clicked: %@",view.annotation.title);
    Restaurant * resto = [APP_DELEGATE getRestaurantByName:view.annotation.title];
    NSLog(@"Clicked: %@",resto.name);
    
    
    if([resto.type isEqualToString:@"Branch"]){
        
        _storeMenu = [[StoreMenuViewController alloc] initWithNibName:@"StoreMenuViewController" bundle:nil];
        _storeMenu.selectedRestaurant = resto;
//        _storeMenu.origin = MAP_ORIGIN;
        [self.navigationController pushViewController:_storeMenu animated:YES];
    }else{
        _restaurantView = [[RestaurantDetailViewController alloc] initWithNibName:@"RestaurantDetailViewController" bundle:nil];
        _restaurantView.selectedRestaurant = resto;
//        _restaurantView.origin = MAP_ORIGIN;
        [self.navigationController pushViewController:_restaurantView animated:YES];
    }
}

- (void)goToFilter{
    [_filter.restaurantArray addObjectsFromArray:self.fetchedRecordsArray];
    
    _filter = [[FilterViewController alloc] initWithNibName:@"FilterViewController" bundle:nil];
    [self.navigationController pushViewController:_filter animated:YES];
    

}


@end
