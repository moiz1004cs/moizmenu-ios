//
//  RestaurantViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/4/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "RestaurantViewController.h"
#import "RestaurantTableCell.h"
#import "StoreMenuViewController.h"
#import "AppDelegate.h"
#import "Restaurant.h"
#import "MoizMenuStyle.h"
#import "MoizMenuUtil.h"
#import "RestaurantDetailViewController.h"

#define APP_DELEGATE                    ((AppDelegate *)[[UIApplication sharedApplication] delegate])

typedef enum{
    sortAlphabetical = 0,
    sortByCusine,
    sortByPriceRange
    
}SortType;

@interface RestaurantViewController ()

@property (nonatomic, strong) NSMutableArray* fetchedRecordsArray;
//@property (nonatomic, strong) NSMutableArray* sortedArray;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) StoreMenuViewController *storeMenuViewController;
@property (nonatomic, strong) RestaurantDetailViewController *restaurantDetail;

//@property (nonatomic) int  sortBy;
//@property (nonatomic) BOOL order;
//@property (nonatomic) BOOL isRestoSelected;

@property (nonatomic) BOOL sortNameType;
@property (nonatomic) BOOL sortCuisineType;
@property (nonatomic) BOOL sortByPriceRange;
@end

@implementation RestaurantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{
    [_tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_nameFilterButton setTitle:@"Name ↓" forState:UIControlStateNormal];
    
    self.fetchedRecordsArray = [[APP_DELEGATE getAllRestaurant]mutableCopy];

    
    [self sortRestaurantByName];
    _sortNameType = YES;
    _sortCuisineType = YES;
    _sortByPriceRange = YES;
    
    [_tableView reloadData];
    [self.nameFilterButton addTarget:self action:@selector(sortRestaurantByName) forControlEvents:UIControlEventTouchUpInside];
    [self.cuisineFilterButton addTarget:self action:@selector(sortRestaurantByCuisine) forControlEvents:UIControlEventTouchUpInside];
    [self.priceRangeFilterButton addTarget:self action:@selector(sortRestaurantByPriceRange) forControlEvents:UIControlEventTouchUpInside];
    
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int counting = [self.fetchedRecordsArray count];
    NSLog(@"Restaurant counrt %d",counting);
    return [self.fetchedRecordsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *CellIdentifier = @"RestaurantTableCell";
    
    RestaurantTableCell *cell = (RestaurantTableCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil){
        cell = [[RestaurantTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }   
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RestaurantTableCell" owner:nil options:nil];

        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[RestaurantTableCell class]])
            {
                cell = (RestaurantTableCell *)currentObject;
                break;
            }
        }
    Restaurant * record = [self.fetchedRecordsArray objectAtIndex:indexPath.row];
    
    NSLog(@"Loading to cell %@",record.name);
    
    cell.restoNameLabel.text = record.name;
    cell.cuisineLabel.text = record.cuisine;
    cell.priceRangeLabel.text = [NSString stringWithFormat:@"%@ - %@", record.priceRangeStart, record.priceRangeEnd];
    
    if([record.type isEqualToString:@"Branch"]){
        
        if([record.originType isEqualToString:@"Salesforce"]){
            cell.restoImageView.image = [UIImage imageWithData:[MoizMenuUtil getImageFromURL:[APP_DELEGATE getMainBranchImage:record.mainRestoId]]];
        }else{
            cell.restoImageView.image = [UIImage imageNamed:[APP_DELEGATE getMainBranchImage:record.mainRestoId]];
        }
        
    }else{
        if([record.originType isEqualToString:@"Salesforce"]){

            cell.restoImageView.image = [UIImage imageWithData:[MoizMenuUtil getImageFromURL:record.imageUrl]];
        }else{
            cell.restoImageView.image = [UIImage imageNamed:record.image];
        }

    }
    

    return cell;
}


#pragma mark UITableViewDelegate functions
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Restaurant *resto = [self.fetchedRecordsArray objectAtIndex:indexPath.row];
    
    if([resto.type isEqualToString:@"Branch"]){
        if (_storeMenuViewController)
            _storeMenuViewController = nil;
        _storeMenuViewController = [[StoreMenuViewController alloc] initWithNibName:@"StoreMenuViewController" bundle:nil];
        _storeMenuViewController.selectedRestaurant = [self.fetchedRecordsArray objectAtIndex:indexPath.row];
        _storeMenuViewController.origin = RESRAURANT_MAIN;
        
        [self.navigationController pushViewController:_storeMenuViewController animated:YES];

    }else{
    
        _restaurantDetail = [[RestaurantDetailViewController alloc] initWithNibName:@"RestaurantDetailViewController" bundle:nil];
        _restaurantDetail.origin = RESRAURANT_MAIN;
        _restaurantDetail.selectedRestaurant = [self.fetchedRecordsArray objectAtIndex:indexPath.row];

        [self.navigationController pushViewController:_restaurantDetail animated:YES];
        
    }

  
}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 75.0;
//}

- (IBAction)filterPressed:(id)sender{
    
}


-(void)sortRestaurantByName{
    [_priceRangeFilterButton setTitle:@"Price Range" forState:UIControlStateNormal];
    [_cuisineFilterButton setTitle:@"Cuisine" forState:UIControlStateNormal];
//    _fetchedRecordsArray = [[NSMutableArray alloc] init];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    [tempArray removeAllObjects];
    [tempArray addObjectsFromArray: _fetchedRecordsArray];
    NSString *key = @"name";
    
    if(_sortNameType){
        [_nameFilterButton setTitle:@"Name ↓" forState:UIControlStateNormal];
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        
        _sortNameType = NO;
    }else{
        [_nameFilterButton setTitle:@"Name ↑" forState:UIControlStateNormal];
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        
        _sortNameType = YES;
    }
    
    [_fetchedRecordsArray removeAllObjects];
    [_fetchedRecordsArray addObjectsFromArray:tempArray];
    [_tableView reloadData];
}

- (void)sortRestaurantByCuisine{
    [_priceRangeFilterButton setTitle:@"Price Range" forState:UIControlStateNormal];
    [_nameFilterButton setTitle:@"Name" forState:UIControlStateNormal];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    [tempArray removeAllObjects];
    [tempArray addObjectsFromArray: _fetchedRecordsArray];
    NSString *key = @"cuisine";
    
    if(_sortCuisineType){
        [_cuisineFilterButton setTitle:@"Cuisine ↓" forState:UIControlStateNormal];
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        _sortCuisineType = NO;
    }else{
        [_cuisineFilterButton setTitle:@"Cuisine ↑" forState:UIControlStateNormal];
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        
        _sortCuisineType = YES;
    }
    [_fetchedRecordsArray removeAllObjects];
    [_fetchedRecordsArray addObjectsFromArray:tempArray];
    [_tableView reloadData];
    
}
- (void)sortRestaurantByPriceRange{
    [_cuisineFilterButton setTitle:@"Cuisine" forState:UIControlStateNormal];
    [_nameFilterButton setTitle:@"Name" forState:UIControlStateNormal];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    [tempArray removeAllObjects];
    [tempArray addObjectsFromArray: _fetchedRecordsArray];
    NSString *key = @"priceRangeStart";
    
    if(_sortByPriceRange){
        [_priceRangeFilterButton setTitle:@"Price Range ↓" forState:UIControlStateNormal];
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        
        _sortByPriceRange = NO;
    }else{
        [_priceRangeFilterButton setTitle:@"Price Range ↑" forState:UIControlStateNormal];
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        
        _sortByPriceRange = YES;
    }
    [_fetchedRecordsArray removeAllObjects];
    [_fetchedRecordsArray addObjectsFromArray:tempArray];
    [_tableView reloadData];
}

-(IBAction)sync:(id)sender{
    if([MoizMenuUtil hasInternetConnection]){
        NSLog(@"Synching");
        [APP_DELEGATE massDeleteCoreData];
        [APP_DELEGATE sfQueryRestaurant];
    }
    self.fetchedRecordsArray = [[APP_DELEGATE getAllRestaurant]mutableCopy];
    
    
    [self sortRestaurantByName];
    _sortNameType = YES;
    _sortCuisineType = YES;
    _sortByPriceRange = YES;

    
    [self.tableView reloadData];
}




@end
