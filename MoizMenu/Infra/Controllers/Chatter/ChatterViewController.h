//
//  ChatterViewController.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/27/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatterViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
-(IBAction)goToAddChatter:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *allChatterButton;
@property (weak, nonatomic) IBOutlet UIButton *atMeChatterButton;

@end
