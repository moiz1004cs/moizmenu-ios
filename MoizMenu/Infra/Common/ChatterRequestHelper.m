//
//  ChatterRequestHelper.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/27/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "ChatterRequestHelper.h"
#import "ChatterObject.h"
#import "ChatterUser.h"

@interface ChatterRequestHelper ()

@property (nonatomic, strong) SFRestRequest *feedItemsRequest;
@property (nonatomic, strong) SFRestRequest *feedItemsMeRequest;

@end

@implementation ChatterRequestHelper

static ChatterRequestHelper *sharedDataHelper = nil;

+(ChatterRequestHelper *)sharedInstance {
    @synchronized([ChatterRequestHelper class])
    {
        if (sharedDataHelper == nil)
            sharedDataHelper= [[ChatterRequestHelper alloc] init];
        return sharedDataHelper;
    }
    return nil;
}

- (id)init {
    self = [super init];
    if (self) {
        _meResults = nil;
        _feedResults = nil;
    }
    return self;
}

-(void)getChatterItems {
    
#ifndef DISABLE_CHATTER
    
    _feedItemsRequest = [SFRestRequest requestWithMethod:SFRestMethodGET path:@"/services/data/v29.0/chatter/feeds/news/me/feed-items" queryParams:nil];
    //SFRestRequest *req = [SFRestRequest requestWithMethod:SFRestMethodGET path:@"/services/data/v28.0/chatter/feeds/news/me/feed-items" queryParams:nil];
    
//    [[SFRestAPI sharedInstance] send:_feedItemsRequest delegate:self];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [[SFRestAPI sharedInstance] send:_feedItemsRequest delegate:self];
    });
#endif
}

-(void)getMeItems {
#ifndef DISABLE_CHATTER
    //SFRestRequest *req = [SFRestRequest requestWithMethod:SFRestMethodGET path:@"/services/data/v28.0/chatter/feeds/to/me/feed-items" queryParams:nil];
    
    //[[SFRestAPI sharedInstance] send:req delegate:self];
    
    _feedItemsMeRequest = [SFRestRequest requestWithMethod:SFRestMethodGET path:@"/services/data/v29.0/chatter/feeds/to/me/feed-items" queryParams:nil];
    [[SFRestAPI sharedInstance] send:_feedItemsMeRequest delegate:self];
#endif
}

- (void)request:(SFRestRequest *)request didLoadResponse:(id)jsonResponse {
    //background thread
    
    if ([NSThread isMainThread]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND,
                                                 (unsigned long)NULL), ^(void) {
            [self request:request didLoadResponse:jsonResponse];
        });
        return;
    }
    
    //NSDictionary *dict = (NSDictionary *)jsonResponse;
    NSDictionary *dict = nil;
    if ([jsonResponse isKindOfClass:[NSDictionary class]])
        dict = (NSDictionary *)jsonResponse;
    else if ([jsonResponse isKindOfClass:[NSData class]])
    {
        
        NSDictionary *jsonObject = nil;
        jsonObject=[NSJSONSerialization
                    JSONObjectWithData:jsonResponse
                    options:NSJSONReadingMutableLeaves
                    error:nil];
        
        if (jsonObject && jsonObject != nil)
            dict = jsonObject;
    }
    else
    {
        NSLog(@"%@ Fail - didLoadResponse (%@) :: %@", @"Chatter", NSStringFromClass([jsonResponse class]), jsonResponse);
    }
    
    
    if (dict)
    {
        NSLog(@"JSON: %@",jsonResponse);
        if ([request.path isEqualToString:@"/services/data/v29.0/chatter/feeds/news/me/feed-items"]) {
            NSLog(@"Hey Im here, chatter");
            _feedResults = [[NSMutableArray alloc] init];
            NSArray *items = [dict objectForKey:@"items"];
            
            for (int i = 0; i < items.count; i++) {
                ChatterObject *post = [[ChatterObject alloc] initWithPostObject:[items objectAtIndex:i]];
                [_feedResults addObject:post];
            }
            NSLog(@"Array Count: %i",  _feedResults.count);
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Finished_loading_chatter_feed" object:nil];
            });
            
        } else {
            _meResults = [[NSMutableArray alloc] init];
            NSArray *items = [dict objectForKey:@"items"];
            
            for (int i = 0; i < items.count; i++) {
                ChatterObject *post = [[ChatterObject alloc] initWithPostObject:[items objectAtIndex:i]];
                [_meResults addObject:post];
                NSLog(@"%@",post.commentsURL);
            }
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"Finished_loading_me_feed" object:nil];
            });
        }
    }
    
}

- (void)request:(SFRestRequest*)request didFailLoadWithError:(NSError*)error {
    NSLog(@"Chatter Request Fail with error: %@",error);
}

- (void)requestDidCancelLoad:(SFRestRequest *)request {
    NSLog(@"Chatter Request Cancelled");
}

- (void)requestDidTimeout:(SFRestRequest *)request {
    NSLog(@"Chatter Request Timeout");
}

-(NSMutableArray*)returnChatterItems {
    NSLog(@"Return the results: ");
    return _feedResults;
}

-(NSMutableArray*)returnMeItems {
    return _meResults;
}
@end