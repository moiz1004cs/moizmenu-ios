//
//  StoreMenuViewController.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/4/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
#import "Menu.h"

@interface StoreMenuViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate, UITableViewDataSource, UINavigationBarDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) Restaurant *selectedRestaurant;
@property (nonatomic, strong) Menu *menu;
@property (nonatomic, strong) NSMutableSet *menuItemSet;

@property (nonatomic) NSString *origin;

@property (nonatomic, strong) IBOutlet UILabel *priceRangeLabel;
@property (nonatomic, strong) IBOutlet UILabel *addressAddress;
@property (nonatomic, strong) IBOutlet UILabel *restoNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *cusineLabel;
@property (nonatomic, strong) IBOutlet UIImageView *ratingImage;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nameButtonSort;
@property (weak, nonatomic) IBOutlet UIButton *priceButtonSort;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;


- (IBAction)goBack:(id)sender;
- (void)sortByPrice;
- (void)sortByName;

@end
