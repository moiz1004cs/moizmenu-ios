//
//  Restaurant.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/21/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "Restaurant.h"
#import "Menu.h"


@implementation Restaurant

@dynamic city;
@dynamic country;
@dynamic cuisine;
@dynamic image;
@dynamic mainRestoId;
@dynamic name;
@dynamic priceRangeEnd;
@dynamic priceRangeStart;
@dynamic rate;
@dynamic restoId;
@dynamic state;
@dynamic street;
@dynamic type;
@dynamic zipcode;
@dynamic menu;
@dynamic originType;
@dynamic imageUrl;
@end
