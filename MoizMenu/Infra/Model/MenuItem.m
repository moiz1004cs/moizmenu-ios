//
//  MenuItem.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/21/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "MenuItem.h"
#import "Menu.h"


@implementation MenuItem

@dynamic category;
@dynamic image;
@dynamic menuId;
@dynamic name;
@dynamic price;
@dynamic menu;
@dynamic menu_item_details;

@end
