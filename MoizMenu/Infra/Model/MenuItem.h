//
//  MenuItem.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/21/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Menu;

@interface MenuItem : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * menuId;
@property (nonatomic, retain) NSString * menu_item_details;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDecimalNumber * price;
@property (nonatomic, retain) NSSet *menu;
@end

@interface MenuItem (CoreDataGeneratedAccessors)

- (void)addMenuObject:(Menu *)value;
- (void)removeMenuObject:(Menu *)value;
- (void)addMenu:(NSSet *)values;
- (void)removeMenu:(NSSet *)values;

@end
