//
//  Menu.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/21/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MenuItem, Restaurant;

@interface Menu : NSManagedObject

@property (nonatomic, retain) NSString * menuId;
@property (nonatomic, retain) NSString * restaurantId;
@property (nonatomic, retain) NSSet *menuItem;
@property (nonatomic, retain) Restaurant *restaurant;
@end

@interface Menu (CoreDataGeneratedAccessors)

- (void)addMenuItemObject:(MenuItem *)value;
- (void)removeMenuItemObject:(MenuItem *)value;
- (void)addMenuItem:(NSSet *)values;
- (void)removeMenuItem:(NSSet *)values;

@end
