//
//  MapAnnotation.m
//  ToDoList
//
//  Created by Abdul Esmail on 1/24/14.
//  Copyright (c) 2014 Abdul Esmail. All rights reserved.
//

#import "MapAnnotation.h"

@implementation MapAnnotation

@synthesize title;
@synthesize subtitle;
@synthesize coordinate;
@synthesize isUrl;

@end
