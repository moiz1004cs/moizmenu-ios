//
//  DownloadImage.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/28/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DownloadImage : NSObject

-(void)downloadSmallProfileImageAsyncFromURL:(NSURL*)url imageView:(UIImageView *)imageView;
- (NSData *)getTheImageData;

@end
