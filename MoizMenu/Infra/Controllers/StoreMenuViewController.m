//
//  StoreMenuViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/4/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "StoreMenuViewController.h"
#import "MenuItemTableCell.h"
#import "MenuItem.h"
#import "RestaurantViewController.h"
#import "AppleMapViewController.h"
#import "RestaurantDetailViewController.h"
#import "MoizMenuUtil.h"
#import "MoizMenuStyle.h"
#import "AppDelegate.h"

#define APP_DELEGATE                    ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define SORT_ASC ASC
#define SORT_DSC DESC

@interface StoreMenuViewController ()
@property (nonatomic, strong) RestaurantViewController *restaurantViewController;
@property (nonatomic, strong) AppleMapViewController *mapController;
@property (nonatomic, strong) RestaurantDetailViewController *detailView;
@property (nonatomic, strong) NSMutableArray *menuItemArray;
@property (nonatomic, strong) NSMutableArray *categoryArray;

@property (nonatomic) BOOL sortNameType;
@property (nonatomic) BOOL sortPriceType;
@property (nonatomic) BOOL sortByCategory;

@end



@implementation StoreMenuViewController

//@synthesize menu = _menu;
//@synthesize selectedRestaurant = _selectedRestaurant;
//@synthesize menuItemSet;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.selectedRestaurant = nibBundleOrNil;
    }
    return self;
}

- (void)viewDidUnload{
    self.selectedRestaurant = NULL;
}

//-(void)viewWillDisappear{
//    self.selectedRestaurant = NULL;
//    [[NSURLCache sharedURLCache] removeAllCachedResponses];
//}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _menuItemArray = [[NSMutableArray alloc] init];
    _categoryArray = [[NSMutableArray alloc] init];
    
    
//    _menuItemArray = [NSMutableArray [self.selectedRestaurant.menu.menuItem allObjects]];
//    [_nameButtonSort setTitle:@"Name ↓" forState:UIControlStateNormal];
    [self.nameButtonSort addTarget:self action:@selector(sortByName) forControlEvents:UIControlEventTouchUpInside];
    [self.priceButtonSort addTarget:self action:@selector(sortByPrice) forControlEvents:UIControlEventTouchUpInside];
    [self.categoryButton addTarget:self action:@selector(sortByCategoryList) forControlEvents:UIControlEventTouchUpInside];
    
    _sortNameType = YES;

    NSLog(@"Populating %@",self.selectedRestaurant.name);
    _restoNameLabel.text = self.selectedRestaurant.name;
    _addressAddress.text = [NSString stringWithFormat:@"%@ %@, %@ %@", self.selectedRestaurant.street, self.selectedRestaurant.city, self.selectedRestaurant.zipcode, self.selectedRestaurant.country];
    _cusineLabel.text = self.selectedRestaurant.cuisine;
    _priceRangeLabel.text = [NSString stringWithFormat:@"%@ - %@", _selectedRestaurant.priceRangeStart, _selectedRestaurant.priceRangeEnd];
    
    if([self.selectedRestaurant.rate  isEqual: @5])
        _ratingImage.image = [UIImage imageNamed:@"5-stars.png"];
    if([self.selectedRestaurant.rate  isEqual: @4])
        _ratingImage.image = [UIImage imageNamed:@"4-stars.png"];
    if([self.selectedRestaurant.rate  isEqual: @3])
        _ratingImage.image = [UIImage imageNamed:@"3-stars.png"];
    if([self.selectedRestaurant.rate  isEqual: @2])
        _ratingImage.image = [UIImage imageNamed:@"2-stars.png"];
    if([self.selectedRestaurant.rate  isEqual: @1])
        _ratingImage.image = [UIImage imageNamed:@"1-star.png"];
    
    self.menuItemSet = [[NSMutableSet alloc]init];
    
    _menuItemArray = [(NSArray *)[self.selectedRestaurant.menu.menuItem allObjects]mutableCopy];
    [self sortRestaurantByName];
    
    NSSet *menuItems = self.selectedRestaurant.menu.menuItem;
    
//    self.navigationBar.a
    
    for (MenuItem *item in menuItems){
        [_menuItemSet addObject:item];
        
//        if(![_categoryArray.lastObject isEqual:item.category]){
//            [_categoryArray addObject:item.category];
//            NSLog(@"Category: %@",item.category);
//        }
    }
    
    
    UIBarButtonItem *showLoc = [[UIBarButtonItem alloc] initWithTitle:@"Loc" style:UIBarButtonItemStylePlain target:self action:@selector(showLocation)];
    
    //    UIBarButtonItem *showMap = [[UIBarButtonItem alloc] initWithTitle:@"Loc" style:UIBarButtonItemStylePlain target:self action:@selector(showOnMap)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:showLoc,nil];

    [self.navigationController setNavigationBarHidden:NO];
    [_tableView reloadData];
}

//- (void)dealloc{
//    self.selectedRestaurant = nil;
//}
//
//- (void)viewWillDisappear:(BOOL)animated{
//    self.selectedRestaurant = nil;
//}

- (void) showLocation{

    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AppleMapViewController * yourView = (AppleMapViewController *)[storyboard instantiateViewControllerWithIdentifier:@"AppleMapViewController"];
    yourView.restaurant = self.selectedRestaurant;
    yourView.origin =  SPECIFIC_RESTO;
    
    [self.navigationController pushViewController:yourView animated:NO];

}


//- (void) viewWillDisappear:(BOOL)animated {
//    
//    [super viewWillDisappear:animated];
////    self.selectedRestaurant.menu.menuItem = menuItemSet;
//    
////    NSError *error = nil;
////    if (![self.bankDetails.managedObjectContext save:&error]) {
////        NSLog(@"Core data error %@, %@", error, [error userInfo]);
////        abort();
////    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_menuItemSet count];
//    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MenuItemTableCell";
    
    MenuItemTableCell *cell = (MenuItemTableCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil){
        cell = [[MenuItemTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"MenuItemTableCell" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[MenuItemTableCell class]])
        {
            cell = (MenuItemTableCell *)currentObject;
            break;
        }
    }
    
//    NSArray *menuItemsArray = [self.selectedRestaurant.menu.menuItem allObjects];
    NSArray *menuItemsArray = _menuItemArray;
    
    cell.menuItemName.text = ((MenuItem *)[menuItemsArray objectAtIndex:indexPath.row]).name;
    cell.menuItemCategory.text = ((MenuItem *)[menuItemsArray objectAtIndex:indexPath.row]).category;
    NSNumber *mydouble = ((MenuItem *)[menuItemsArray objectAtIndex:indexPath.row]).price;
//    NSString *imageMenu = ((MenuItem *)[menuItemsArray objectAtIndex:indexPath.row]).image;
    cell.menuItemPrice.text = [NSString stringWithFormat:@"P %@", mydouble];
    
    NSString *imageNameURL = ((MenuItem *)[menuItemsArray objectAtIndex:indexPath.row]).image;
//    NSLog(@"Image URL: %@",imageNameURL);
    if(![self.selectedRestaurant.originType isEqualToString:@"PList"]){
        cell.menuItemImage.image = [UIImage imageWithData:[MoizMenuUtil getImageFromURL:imageNameURL]];
        
    }else{
        cell.menuItemImage.image = [UIImage imageNamed:((MenuItem *)[menuItemsArray objectAtIndex:indexPath.row]).image];
    }
    return cell;
}


#pragma mark UITableViewDelegate functions
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 75.0;
//}
- (IBAction)filterPressed:(id)sender{
    
}

- (IBAction)goBack:(id)sender{
    
    
//    [self.view addSubview:_restaurantViewController.view];
    
    if([self.origin isEqualToString:MAP_ORIGIN]){
        _mapController = [[AppleMapViewController alloc] initWithNibName:@"AppleMapViewController" bundle:nil];
        //        _storeMenu.selectedRestaurant = resto;
        [self.navigationController pushViewController:_mapController animated:YES];
        
    }else if([self.origin isEqualToString:RESTAURANT_DETAIL_VIEW_ORIGIN]){
//        AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
        Restaurant * resto;
        if([self.selectedRestaurant.type isEqualToString:@"Branch"]){
            resto = [APP_DELEGATE getRestaurantById:self.selectedRestaurant.mainRestoId];
        }else{
            resto = [APP_DELEGATE getRestaurantById:self.selectedRestaurant.restoId];
        }
        
        
//        float height = [MoizMenuUtil windowHeight] - (64*2);
        _detailView = [[RestaurantDetailViewController alloc] initWithNibName:@"RestaurantDetailViewController" bundle:nil];
        _detailView.selectedRestaurant = resto;
//        [self.navigationController pushViewController:<#(UIViewController *)#> animated:<#(BOOL)#>]
//
//        [self.view addSubview:_detailView.view];
//        
//        [_detailView.view setFrame:CGRectMake(320,0, 320, height)];
//        
//        NSLog(@"rect %@",NSStringFromCGRect(_detailView.view.frame));
//        
//        [UIView animateWithDuration:0.3 animations:^{
//            //        self.transform = CGAffineTransformMakeScale(1.25, 0.75);
//            [_detailView.view setFrame:CGRectMake(0,0, 320, height)];
//        } completion:^(BOOL isDone){
//            
//        }];
        
    }else{
        float height = [MoizMenuUtil windowHeight] - (64*2);
        _restaurantViewController = [[RestaurantViewController alloc] initWithNibName:@"RestaurantViewController" bundle:nil];
        
        [self.view addSubview:_restaurantViewController.view];
        
        [_restaurantViewController.view setFrame:CGRectMake(320,0, 320, height)];
        
        NSLog(@"rect %@",NSStringFromCGRect(_restaurantViewController.view.frame));
        
        [UIView animateWithDuration:0.3 animations:^{
            //        self.transform = CGAffineTransformMakeScale(1.25, 0.75);
            [_restaurantViewController.view setFrame:CGRectMake(0,0, 320, height)];
        } completion:^(BOOL isDone){
            
        }];
    }
    

}

-(void)sortRestaurantByName{
    [_categoryButton setTitle:@"Category ↓" forState:UIControlStateNormal];
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    [tempArray removeAllObjects];
    [tempArray addObjectsFromArray: _menuItemArray];
    
    NSString *key = @"category";
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
    NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
    //    [brandDescriptor release];
    [tempArray removeAllObjects];
    tempArray = (NSMutableArray*)sortedArray;
    [_menuItemArray removeAllObjects];
    [_menuItemArray addObjectsFromArray:tempArray];
    
    _sortByCategory = NO;

}
- (void)sortByPrice{
    //    _fetchedRecordsArray = [[NSMutableArray alloc] init];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    [_nameButtonSort setTitle:@"Name" forState:UIControlStateNormal];
    [_categoryButton setTitle:@"Category" forState:UIControlStateNormal];
    [tempArray removeAllObjects];
    [tempArray addObjectsFromArray: _menuItemArray];
    if(_sortPriceType){
        [_priceButtonSort setTitle:@"Price ↓" forState:UIControlStateNormal];

    
        NSString *key = @"price";
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        [_menuItemArray removeAllObjects];
        [_menuItemArray addObjectsFromArray:tempArray];
        
        _sortPriceType = NO;
    }else{
        
        [_priceButtonSort setTitle:@"Price ↑" forState:UIControlStateNormal];
        
        NSString *key = @"price";
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        [_menuItemArray removeAllObjects];
        [_menuItemArray addObjectsFromArray:tempArray];
        _sortPriceType = YES;
    }
    [_tableView reloadData];
}
- (void)sortByName{
    //    _fetchedRecordsArray = [[NSMutableArray alloc] init];
    [_priceButtonSort setTitle:@"Price" forState:UIControlStateNormal];
    [_categoryButton setTitle:@"Category" forState:UIControlStateNormal];
    
    if(_sortNameType){
        [_nameButtonSort setTitle:@"Name ↓" forState:UIControlStateNormal];
    
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        [tempArray removeAllObjects];
        [tempArray addObjectsFromArray: _menuItemArray];
    
        NSString *key = @"name";
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        [_menuItemArray removeAllObjects];
        [_menuItemArray addObjectsFromArray:tempArray];
        
        _sortNameType = NO;
    }else{
        [_nameButtonSort setTitle:@"Name ↑" forState:UIControlStateNormal];
        
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        [tempArray removeAllObjects];
        [tempArray addObjectsFromArray: _menuItemArray];
        
        NSString *key = @"name";
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        [_menuItemArray removeAllObjects];
        [_menuItemArray addObjectsFromArray:tempArray];
        _sortNameType = YES;

    }

    [_tableView reloadData];

}

-(void)sortByCategoryList{
    //    _fetchedRecordsArray = [[NSMutableArray alloc] init];
    [_priceButtonSort setTitle:@"Price" forState:UIControlStateNormal];
    [_nameButtonSort setTitle:@"Name" forState:UIControlStateNormal];
    
    if(_sortByCategory){
        [_categoryButton setTitle:@"Category ↓" forState:UIControlStateNormal];
        
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        [tempArray removeAllObjects];
        [tempArray addObjectsFromArray: _menuItemArray];
        
        NSString *key = @"category";
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        [_menuItemArray removeAllObjects];
        [_menuItemArray addObjectsFromArray:tempArray];
        
        _sortByCategory = NO;
    }else{
        [_categoryButton setTitle:@"Category ↑" forState:UIControlStateNormal];
        
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        [tempArray removeAllObjects];
        [tempArray addObjectsFromArray: _menuItemArray];
        
        NSString *key = @"category";
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        [_menuItemArray removeAllObjects];
        [_menuItemArray addObjectsFromArray:tempArray];
        _sortByCategory = YES;
        
    }
    
    [_tableView reloadData];
}


@end
