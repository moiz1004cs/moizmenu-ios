//
//  RestaurantDetailViewController.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/14/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"

@interface RestaurantDetailViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate, UITableViewDataSource, UINavigationBarDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong)Restaurant *selectedRestaurant;
@property (nonatomic) NSString *origin;

@property (nonatomic, strong) IBOutlet UILabel *cuisine;
@property (nonatomic, strong) IBOutlet UILabel *priceRange;
@property (nonatomic, strong) IBOutlet UILabel *type;
@property (nonatomic, strong) IBOutlet UILabel *address;
@property (nonatomic, strong) IBOutlet UIImageView *ratingImage;


@property (weak, nonatomic) IBOutlet UILabel *branchNameFilterButton;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *branchesLabelButton;

@property (weak, nonatomic) IBOutlet UINavigationItem *navigationTitle;


//-(IBAction)goToMenu:(id)sender;
-(IBAction)goBack:(id)sender;

@end
