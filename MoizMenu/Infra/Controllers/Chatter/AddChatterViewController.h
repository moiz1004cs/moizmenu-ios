//
//  AddChatterViewController.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/28/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddChatterViewController : UIViewController<UITextViewDelegate>

-(IBAction)goToBack:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *postBodyTextField;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end
