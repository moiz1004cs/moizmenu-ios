//
//  MapAnnotation.h
//  ToDoList
//
//  Created by Abdul Esmail on 1/24/14.
//  Copyright (c) 2014 Abdul Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapAnnotation : NSObject <MKAnnotation>{
    
    NSString *title;
    NSString *subtitle;
    NSString *note;
    CLLocationCoordinate2D coordinate;
    Boolean *isUrl;
}

@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString * subtitle;
@property (nonatomic, assign)CLLocationCoordinate2D coordinate;
@property (nonatomic, assign)Boolean *isUrl;

@end
