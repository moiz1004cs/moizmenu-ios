//
//  ChatterImage.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/26/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatterImage : NSObject

-(void)downloadSmallProfileImageAsyncFromURL:(NSURL*)url;

@end
