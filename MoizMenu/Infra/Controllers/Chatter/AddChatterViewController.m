//
//  AddChatterViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/28/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "AddChatterViewController.h"
#import "ChatterViewController.h"
#import "MoizMenuUtil.h"

@interface AddChatterViewController ()


@property (nonatomic, strong) ChatterViewController *chatterView;

@end

@implementation AddChatterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _postBodyTextField.text = @"";
//    _postBodyTextField.keyboardType =
//    [self]
    UIBarButtonItem *showLoc = [[UIBarButtonItem alloc] initWithTitle:@"Post" style:UIBarButtonItemStylePlain target:self action:@selector(postChatter)];
    
    //    UIBarButtonItem *showMap = [[UIBarButtonItem alloc] initWithTitle:@"Loc" style:UIBarButtonItemStylePlain target:self action:@selector(showOnMap)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:showLoc,nil];
}

- (void)postChatter{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goToBack:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

//-(IBAction)backToChat:(id)sender

- (void)textFieldDidBeginEditing:(UITextView *)textField
{
    _postBodyTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextView *)textField
{
    _postBodyTextField = nil;
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    return NO;
}


@end
