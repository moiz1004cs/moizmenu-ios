//
//  ChatterObject.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/27/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatterUser.h"
#import "SFRestAPI.h"

@interface ChatterObject : NSObject <SFRestDelegate> {
    NSMutableArray *feedResults;
}
@property (strong, nonatomic) NSDictionary *postDict;
@property (strong, nonatomic) ChatterUser *user;
@property (strong, nonatomic) NSString *postContent;
@property (strong, nonatomic) NSString *relativeTime;
@property (strong, nonatomic) NSURL *commentsURL;
@property (strong, nonatomic) NSURL *likeURL;
@property (strong, nonatomic) NSString *numLikes;
@property (strong, nonatomic) NSString *numComments;

-(id)initWithPostObject:(NSDictionary*)post;
@end
