//
//  ChatterObject.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/27/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "ChatterObject.h"
#import "ChatterUser.h"
#import "SFRestAPI.h"

@implementation ChatterObject
@synthesize postDict,user;

-(id)initWithPostObject:(NSDictionary*)post {
    
    ChatterObject *postObj = [[ChatterObject alloc] init];
    postObj.postDict = post;
    postObj.user = [[ChatterUser alloc] initWithUserDict:[post objectForKey:@"actor"]];
    postObj.relativeTime = [post objectForKey:@"relativeCreatedDate"];
    NSDictionary *postBody = [post objectForKey:@"body"];
    postObj.postContent = [postBody objectForKey:@"text"];
    NSDictionary *commentsBody = [post objectForKey:@"comments"];
    postObj.commentsURL = [NSURL URLWithString:[commentsBody objectForKey:@"currentPageUrl"]];
    postObj.numComments = [commentsBody objectForKey:@"total"];
    
    NSDictionary *likeBody = [post objectForKey:@"likes"];
    postObj.likeURL = [NSURL URLWithString:[likeBody objectForKey:@"currentPageUrl"]];
    postObj.numLikes = [likeBody objectForKey:@"total"];
    
    
    return postObj;
}

-(void)getChatterItems {
    
#ifndef DISABLE_CHATTER
    SFRestRequest *req = [SFRestRequest requestWithMethod:SFRestMethodGET path:@"/services/data/v29.0/chatter/feeds/news/me/feed-items" queryParams:nil];
    
    [[SFRestAPI sharedInstance] send:req delegate:self];
#endif
}

@end
