//
//  AppleMapViewController.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/11/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "FilterViewController.h"

@interface AppleMapViewController : UIViewController<MKMapViewDelegate, CLLocationManagerDelegate, UIPopoverControllerDelegate, GMSMapViewDelegate, FilterViewDelegate>

//@property (strong, nonatomic)IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

//@property (nonatomic, strong) IBOutlet UIButton *filterButton;
@property (nonatomic, strong) IBOutlet UIButton *nameField;
@property (nonatomic, strong) IBOutlet UIButton *priceRangeField;
@property (nonatomic, strong) IBOutlet UIButton *cuisineField;
//@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;

@property (nonatomic) NSString *origin;

@property (nonatomic, strong) NSString *filterRestoName;
@property (nonatomic, strong) NSString *filterCuisine;
@property (nonatomic, strong) NSString *filterPriceStarRange;
@property (nonatomic, strong) NSString *filterPRiceEndRange;

@property (nonatomic, strong) Restaurant *restaurant;

- (void)goToFilter;
@end
