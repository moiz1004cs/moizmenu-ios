//
//  SettingViewController.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/19/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *loginBUtton;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UILabel *fullName;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UILabel *profile;
@property (strong, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UIImageView *imagePrfile;


-(IBAction)appLogout:(id)sender;
-(IBAction)sync:(id)sender;

@end
