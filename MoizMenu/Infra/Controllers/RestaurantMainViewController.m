//
//  RestaurantMainViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/4/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "RestaurantMainViewController.h"


@interface RestaurantMainViewController ()

@property (nonatomic, strong) RestaurantViewController *restaurantViewController;
@end

@implementation RestaurantMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //load RestaurnatViewController
    _restaurantViewController = [[RestaurantViewController alloc]initWithNibName:nil bundle:nil];
//    [_restaurantViewController setDelegate:self];
    [_mainView addSubview:_restaurantViewController.view];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
