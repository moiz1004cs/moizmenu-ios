//
//  RestaurantViewController.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/4/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
//@property (weak, nonatomic) IBOutlet UIButton *nameFilterButton;
//@property (weak, nonatomic) IBOutlet UIButton *cuisineFilterButton;
//@property (weak, nonatomic) IBOutlet UIButton *priceRangeFilterButto;
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property (nonatomic, strong) IBOutlet UIButton *nameFilterButton;
//@property (nonatomic, strong) IBOutlet UIButton *cuisineFilterButton;
//@property (nonatomic, strong) IBOutlet UIButton *priceRangeFilterButto;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nameFilterButton;
@property (weak, nonatomic) IBOutlet UIButton *cuisineFilterButton;
@property (weak, nonatomic) IBOutlet UIButton *priceRangeFilterButton;

@property (nonatomic) NSString *origin;


- (IBAction)filterPressed:(id)sender;
- (void)sortRestaurantByName;
- (void)sortRestaurantByCuisine;
- (void)sortRestaurantByPriceRange;

-(IBAction)sync:(id)sender;

@end
