//
//  AppDelegate.m
//  ziomMenu
//
//  Created by Abdul Esmail on 1/30/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "Restaurant.h"
#import "Menu.h"
#import "MenuItem.h"
#import "MoizMenuUtil.h"
#import "User.h"

#import "InitialViewController.h"

#import "SFAccountManager.h"
#import "SFAuthenticationManager.h"
#import "SFPushNotificationManager.h"
#import "SFOAuthInfo.h"
#import "SFLogger.h"
#import "SFIdentityData.h"
#import "ChatterRequestHelper.h"

#define APP_DELEGATE                    ((AppDelegate *)[[UIApplication sharedApplication] delegate])



// Fill these in when creating a new Connected Application on Force.com
static NSString * const RemoteAccessConsumerKey = @"3MVG9Y6d_Btp4xp5wCSX4B.3qSokhkAjuStIQdL2jAvmk.Mb6OlO9xfPFje1boi1QKiGgf5XkywHVst1r1Vla";
static NSString * const OAuthRedirectURI        = @"sfdc://success";

@interface AppDelegate ()

//@property (nonatomic, strong) SFRestaurant *sfRestaurant;

@property (strong, nonatomic) InitialViewController *initialAppViewController;

/**
 * Success block to call when authentication completes.
 */
@property (nonatomic, copy) SFOAuthFlowSuccessCallbackBlock initialLoginSuccessBlock;

/**
 * Failure block to calls if authentication fails.
 */
@property (nonatomic, copy) SFOAuthFlowFailureCallbackBlock initialLoginFailureBlock;

/**
 * Handles the notification from SFAuthenticationManager that a logout has been initiated.
 * @param notification The notification containing the details of the logout.
 */
- (void)logoutInitiated:(NSNotification *)notification;

/**
 * Handles the notification from SFAuthenticationManager that the login host has changed in
 * the Settings application for this app.
 * @param The notification whose userInfo dictionary contains:
 *        - kSFLoginHostChangedNotificationOriginalHostKey: The original host, prior to host change.
 *        - kSFLoginHostChangedNotificationUpdatedHostKey: The updated (new) login host.
 */
- (void)loginHostChanged:(NSNotification *)notification;

/**
 * Convenience method for setting up the main UIViewController and setting self.window's rootViewController
 * property accordingly.
 */
- (void)setupRootViewController;

/**
 * (Re-)sets the view state when the app first loads (or post-logout).
 */
- (void)initializeAppViewState;

- (void)registerDefaultsFromSettingsBundle;

@property (nonatomic)BOOL queryRestaurant;
@property (nonatomic, strong) Restaurant *restaurant;
@property (nonatomic) NSMutableArray *salesforceArray;
@property (nonatomic) BOOL DEFAULT_LOGIN;


@end


@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize window = _window;
@synthesize initialLoginSuccessBlock = _initialLoginSuccessBlock;
@synthesize initialLoginFailureBlock = _initialLoginFailureBlock;


- (id)init
{
    self = [super init];
    if (self) {
        
//        [self registerDefaultsFromSettingsBundle];
//        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//        NSString *userDefault  = [prefs stringForKey:@"local_login"];
//        if([userDefault isEqualToString:@"1"]){
//            [self initializeAppViewState];
//            [self loadDataFromPListToCoreData];
//        }

        [SFLogger setLogLevel:SFLogLevelDebug];
        
        // These SFAccountManager settings are the minimum required to identify the Connected App.
        [SFAccountManager setClientId:RemoteAccessConsumerKey];
        [SFAccountManager setRedirectUri:OAuthRedirectURI];
        [SFAccountManager setScopes:[NSSet setWithObjects:@"api", nil]];
        
        // Logout and login host change handlers.
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutInitiated:) name:kSFUserLogoutNotification object:[SFAuthenticationManager sharedManager]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginHostChanged:) name:kSFLoginHostChangedNotification object:[SFAuthenticationManager sharedManager]];
        
        // Blocks to execute once authentication has completed.  You could define these at the different boundaries where
        // authentication is initiated, if you have specific logic for each case.
        __weak AppDelegate *weakSelf = self;
        self.initialLoginSuccessBlock = ^(SFOAuthInfo *info) {
            [weakSelf setupRootViewController];
        };
        self.initialLoginFailureBlock = ^(SFOAuthInfo *info, NSError *error) {
            [[SFAuthenticationManager sharedManager] logout];
        };
        
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSFUserLogoutNotification object:[SFAuthenticationManager sharedManager]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSFLoginHostChangedNotification object:[SFAuthenticationManager sharedManager]];
    
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Load data from pList
//    [self getAllRestaurant];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    [self initializeAppViewState];
    [self registerDefaultsFromSettingsBundle];
    [GMSServices provideAPIKey:@"AIzaSyCQf3H1rRiO6066DibQll96yUgKh6hvftM"];
    [self massDeleteCoreData];
    [self deleteAllUsers];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *userDefault  = [prefs stringForKey:@"local_login"];
    
    NSLog(@"User Default: %@",userDefault);
    
    if([userDefault isEqualToString:@"1"]){
        [self initializeAppViewState];
        [self loadDataFromPListToCoreData];
    }else{
        [[SFAuthenticationManager sharedManager] loginWithCompletion:self.initialLoginSuccessBlock failure:self.initialLoginFailureBlock];
        [self sfQueryRestaurant];
//        [self loadDataFromPListToCoreData];
//        [self sfQueryAttachments];
    }
    

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)loadDataFromPListToCoreData
{
    // Read plist from bundle and get Root Dictionary out of it
    NSDictionary *dictRoot = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Restaurants" ofType:@"plist"]];
    
    // Your dictionary contains an array of dictionary
    // Now pull an Array out of it.
    NSArray *arrayList = [NSArray arrayWithArray:[dictRoot objectForKey:@"Restaurants"]];
    
    
    // Now a loop through Array to fetch single Item from catList which is Dictionary
    [arrayList enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop) {
        // Fetch Single Item
        Restaurant * newEntry = [NSEntityDescription insertNewObjectForEntityForName:@"Restaurant"
                                                              inManagedObjectContext:self.managedObjectContext];
        
        newEntry.restoId = [obj valueForKey:@"Id"];
        newEntry.name = [obj valueForKey:@"Name"];
        newEntry.priceRangeStart = [obj valueForKey:@"PriceRangeStart"];
        newEntry.priceRangeEnd = [obj valueForKey:@"PriceRangeEnd"];
        newEntry.type = [obj valueForKey:@"Type"];
        newEntry.rate = [obj valueForKey:@"Rate"];
        newEntry.cuisine = [obj valueForKey:@"Cuisine"];
        newEntry.street = [obj valueForKey:@"Street"];
        newEntry.city = [obj valueForKey:@"City"];
        newEntry.country = [obj valueForKey:@"Country"];
        newEntry.zipcode = [obj valueForKey:@"ZipCode"];
        newEntry.state = [obj valueForKey:@"State"];
        newEntry.originType = @"PList";
        
        if([[obj valueForKey:@"Type" ] isEqualToString:@"Main"]){
            newEntry.image = [obj valueForKey:@"Image"];
        }
        newEntry.mainRestoId = [obj valueForKey:@"MainRestoId"];
    
        newEntry.menu = [self getMenuById:[obj valueForKey:@"Id"]];
        
        NSLog(@"Saving %@",[obj valueForKey:@"Name"]);

    }];
   
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
}

-(Menu *)getMenuById:(NSString *)restoId{
    
    // Read plist from bundle and get Root Dictionary out of it
    NSDictionary *dictRoot = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Menu" ofType:@"plist"]];
    
    
    NSArray *arrayList = [NSArray arrayWithArray:[dictRoot objectForKey:restoId]];
//    Menu * entryObj;
    Menu *newEntry = [NSEntityDescription insertNewObjectForEntityForName:@"Menu"
                                                   inManagedObjectContext:self.managedObjectContext];
    
//    Menu * newEntry;
    // Now a loop through Array to fetch single Item from catList which is Dictionary
    [arrayList enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop) {
        // Fetch Single Item
        
        if([restoId isEqualToString:[obj valueForKey:@"RestoId"]]){
        
            newEntry.restaurantId  = [obj valueForKey:@"RestoId"];
            newEntry.menuId = [obj valueForKey:@"MenuId"];
            
            NSLog(@"Loading menu %@", [obj valueForKey:@"MenuId"]);
            
            newEntry.menuItem = [NSSet setWithSet:[self getMenuItemsByMenuId:[obj valueForKey:@"MenuId"]]];

            
        }
    }];
    
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    return newEntry;
    
}

-(NSSet *)getMenuItemsByMenuId:(NSString *)menuId{
    NSMutableSet* menuItems = [NSMutableSet set];
    NSDictionary *dicRoot = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"MenuItems" ofType:@"plist"]];
    
    NSArray *arrayList = [NSArray arrayWithArray:[dicRoot objectForKey:menuId]];
    //    MenuItem *menuItemObj;
//
    
    
    [arrayList enumerateObjectsUsingBlock:^(id obj, NSUInteger index, BOOL *stop) {
        // Fetch Single Item
        
        MenuItem *newEntry = [NSEntityDescription insertNewObjectForEntityForName:@"MenuItem"
                                                           inManagedObjectContext:self.managedObjectContext];
        
            newEntry.menuId  = [obj valueForKey:@"MenuItemId"];
            newEntry.category = [obj valueForKey:@"Category"];
            newEntry.name = [obj valueForKey:@"Name"];
            newEntry.price = [obj valueForKey:@"Price"];
            newEntry.image = [obj valueForKey:@"Image"];
            
            //            entryObj.menuId = newEntry.menuId;
            //            entryObj.restaurantId = newEntry.restaurantId;
            
            [menuItems addObject:newEntry];
            NSLog(@"Loading item id %@", [obj valueForKey:@"MenuItemId"]);
//            arrayOfMenuItems[index] = newEntry;
//        }
    }];
    
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    return  menuItems;
}

-(NSArray *)getAllRestaurant
{
    
    
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSLog(@"Querying the restaurant entity for the MoizMenu sqlite");
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Restaurant"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    // Returning Fetched Records
    return fetchedRecords;
}

-(NSArray *)getAllCuisine
{
    
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSLog(@"Querying the quisines entity from the MoizMenu sqlite");
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Restaurant"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:@"cuisine"]];
    [fetchRequest setReturnsDistinctResults:YES];
    [fetchRequest setResultType:NSDictionaryResultType];
    
    NSError* error;
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
    
//    if(fetchedRecords.count > 0){
    
        for (NSDictionary *dict in fetchedRecords) {
            if([dict objectForKey:@"cuisine"] != nil){
                [arr addObject:[dict objectForKey:@"cuisine"]];
            }
        }
//    }
    
    return arr;
    
}
-(NSArray *)getAllPriceRangeStart
{
    
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSLog(@"Querying the priceRangeStart entity from the MoizMenu sqlite");
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Restaurant"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:@"priceRangeStart"]];
    [fetchRequest setReturnsDistinctResults:YES];
    [fetchRequest setResultType:NSDictionaryResultType];
    
    NSError* error;
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
    
    for (NSDictionary *dict in fetchedRecords) {
        if([dict objectForKey:@"priceRangeStart"] != nil){
            NSString * str =[dict objectForKey:@"priceRangeStart"];
            [arr addObject:str];
        }
        
    }
    
    return arr;
    
}

-(NSArray *)getAllPriceRangeEnd
{
    
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSLog(@"Querying the priceRangeEnd entity from the MoizMenu sqlite");
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Restaurant"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:@"priceRangeEnd"]];
    [fetchRequest setReturnsDistinctResults:YES];
    [fetchRequest setResultType:NSDictionaryResultType];
    
    NSError* error;
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
    
    for (NSDictionary *dict in fetchedRecords) {
        if([dict objectForKey:@"priceRangeEnd"] != nil){
            NSString * str = [dict objectForKey:@"priceRangeEnd"];
            [arr addObject:str];
        }
        
    }
    
    return arr;
    
}

-(NSArray *)getAllBranchesByRestoId:(NSString *) restoId
{
    
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSLog(@"Querying the restaurant branches %@ for the MoizMenu sqlite",restoId);
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Restaurant"
                                              inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"mainRestoId == %@", restoId];
    [fetchRequest setPredicate:predicate];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    // Returning Fetched Records
    if(fetchRequest != nil){
        NSLog(@"Counted: %lu", (unsigned long)[fetchedRecords count]);
    }
    return fetchedRecords;
}

//NSCompundpredicate

-(NSArray *)getRestaurantByFilter:(NSString *) restoName cuisineName:(NSString *)cuisineName priceRangeStart:(NSString *)priceRangeStart priceRangeEnd:(NSString *)priceRangeEnd
{
    
    // initializing NSFetchRequest
    NSPredicate *predicate;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSLog(@"Querying the restaurant %@ for the MoizMenu sqlite to plot on map",restoName);
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Restaurant"
                                              inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:entity];
//    NSPredicate *restoNamePredicate;
//    
//    if(![restoName isEqualToString:@""] && ![cuisineName isEqualToString:@""]){
//        restoNamePredicate  = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ && cuisine CONTAINS[cd] %@ ", restoName, cuisineName];
//    }else if(![restoName isEqualToString:@""] && [cuisineName isEqualToString:@""]){
//        restoNamePredicate  = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ ", restoName];
//    }else if([restoName isEqualToString:@""] && ![cuisineName isEqualToString:@""]){
//        restoNamePredicate  = [NSPredicate predicateWithFormat:@"cuisine CONTAINS[cd] %@ ", cuisineName];
//    }else{
//        restoNamePredicate = nil;
//    }
//    
    
//    NSPredicate *cuisinePredicate = [NSPredicate predicateWithFormat:@"cuisine CONTAINS[cd] %@", cuisineName];
//    NSPredicate *priceRangeStartPredicate = [NSPredicate predicateWithFormat:@"priceRangeStart >= %@ && priceRangeEnd <= %@",priceRangeStart, priceRangeEnd];

//    NSArray *subPredicates = [[NSArray alloc] initWithObjects:restoNamePredicate,priceRangeStartPredicate, nil];
    if(![restoName isEqualToString:@""] && [cuisineName isEqualToString:@""] && [priceRangeStart isEqualToString:@""]){
        predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", restoName];
    }else if([restoName isEqualToString:@""] && ![cuisineName isEqualToString:@""] && [priceRangeStart isEqualToString:@""]){
        predicate = [NSPredicate predicateWithFormat:@"cuisine CONTAINS[cd] %@", cuisineName];
    }else if(![restoName isEqualToString:@""] && ![cuisineName isEqualToString:@""] && [priceRangeStart isEqualToString:@""]){
        predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ && cuisine CONTAINS[cd] %@",restoName, cuisineName];
    }else if([cuisineName isEqualToString:@""] && ![restoName isEqualToString:@""] && ![priceRangeStart isEqualToString:@""]){
        predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@ AND (priceRangeStart >= %@ AND priceRangeEnd <= %@)", restoName, priceRangeStart, priceRangeEnd];
    }else if(![cuisineName isEqualToString:@""] && ![restoName isEqualToString:@""] && ![priceRangeStart isEqualToString:@""]){
        predicate = [NSPredicate predicateWithFormat:@"cuisine CONTAINS[cd] %@ AND name CONTAINS[cd] %@ AND priceRangeStart >= %@ AND priceRangeEnd <= %@",cuisineName, restoName, priceRangeStart, priceRangeEnd];
    }else if([restoName isEqualToString:@""] && [cuisineName isEqualToString:@""] && ![priceRangeStart isEqualToString:@""]){
        predicate = [NSPredicate predicateWithFormat:@" priceRangeStart >= %@ AND priceRangeEnd <= %@", priceRangeStart, priceRangeEnd];
    }
//
//    predicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    
   
    [fetchRequest setPredicate:predicate];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    // Returning Fetched Records
    if(fetchRequest != nil){
        NSLog(@"Counted: %lu", (unsigned long)[fetchedRecords count]);
    }
    return fetchedRecords;
}

-(Restaurant *)getRestaurantById:(NSString *)restoId{
    // initializing NSFetchRequest
    Restaurant *restaurant;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSLog(@"Querying %@ for the MoizMenu sqlite",restoId);
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Restaurant"
                                              inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"restoId == %@", restoId];
    [fetchRequest setPredicate:predicate];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if(fetchRequest != nil){
        for (Restaurant *resto in fetchedRecords) {
            if([resto.restoId isEqualToString:restoId]){
                NSLog(@"Done query on: %@", resto.name);
                
                restaurant = resto;
                
                NSLog(@"Returning %@", restaurant.name);
            }
        }
    }
    
    return restaurant;
}

-(Restaurant *)getRestaurantByName:(NSString *)restoName{
    // initializing NSFetchRequest
    Restaurant *restaurant;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSLog(@"Querying %@ for the MoizMenu sqlite",restoName);
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Restaurant"
                                              inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", restoName];
    [fetchRequest setPredicate:predicate];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if(fetchRequest != nil){
    for (Restaurant *resto in fetchedRecords) {
        if([resto.name isEqualToString:restoName]){
            NSLog(@"Done query on: %@", resto.name);
            
            restaurant = resto;
            
            NSLog(@"Returning %@", restaurant.name);
        }
    }
    }
    
    return restaurant;
}

-(NSString *)getMainBranchImage:(NSString *)restoId{
    NSString *restaurantImage;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSLog(@"Querying %@ for the MoizMenu sqlite",restoId);
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Restaurant"
                                              inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"restoId == %@", restoId];
    [fetchRequest setPredicate:predicate];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if(fetchRequest != nil){
        for (Restaurant *resto in fetchedRecords) {
            if([resto.restoId isEqualToString:restoId]){
                NSLog(@"Done query on: %@", resto.name);
                
                if([resto.originType isEqualToString:@"Salesforce"]){
                    restaurantImage = resto.imageUrl;
                }else{
                    restaurantImage = resto.image;
                }
                
                
                
                NSLog(@"Returning %@", restaurantImage);
            }
        }
    }
    
    return restaurantImage;
}

-(User *)getUserProfile{
    User *userInfo;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User"
                                              inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"status == 'Active'"];
    [fetchRequest setPredicate:predicate];
    NSError* error;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if(fetchRequest != nil){
        for (User *user in fetchedRecords) {
                NSLog(@"Done query on: User");
                userInfo = user;
        }
    }
    
    return userInfo;
}



- (NSManagedObjectContext *)managedObjectContext
{
    /*if (_managedObjectContext != nil) {
     return _managedObjectContext;
     }*/
    
    if (_managedObjectContext){
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    
    if (coordinator){
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
        [_managedObjectContext setUndoManager:nil];
    }
    
    NSLog(@"_managedObjectContext: %@", _managedObjectContext);
    
    return _managedObjectContext;
    
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    //    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MoizMenu" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSLog(@"Query for the Menu sqlite");
    NSURL *storeUrl = [NSURL fileURLWithPath: [[MoizMenuUtil applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"MoizMenu.sqlite"]];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil URL:storeUrl options:nil error:&error]) {
        /*Error for store creation should be handled in here*/
    }
    
    return _persistentStoreCoordinator;
}



#pragma mark - Private methods

- (void)initializeAppViewState
{
    NSString *userName = [SFAccountManager sharedInstance].idData.username;
    NSLog(@"Username: %@",userName);
    self.window.rootViewController = self.rootVC;
    [self.window makeKeyAndVisible];
    
    
}

- (void)setupRootViewController
{
    NSString *userName = [SFAccountManager sharedInstance].idData.username;
    NSString *profile = [SFAccountManager sharedInstance].idData.userType;
    NSString *name = [SFAccountManager sharedInstance].idData.firstName;
    NSString *lastName = [SFAccountManager sharedInstance].idData.lastName;
    NSString *email = [SFAccountManager sharedInstance].idData.email;
    NSString *imageUrl = [NSString stringWithFormat:@"%@",[SFAccountManager sharedInstance].idData.pictureUrl];
    
    User * newEntry = [NSEntityDescription insertNewObjectForEntityForName:@"User"
                                                          inManagedObjectContext:self.managedObjectContext];
    
    newEntry.userName = userName;
    newEntry.profile = profile;
    newEntry.firstName = name;
    newEntry.lastName = lastName;
    newEntry.email = email;
    newEntry.status = @"Active";
    newEntry.pictureUrl = imageUrl;
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }

    NSLog(@"Username: %@",userName);
    NSLog(@"User Type: %@", profile);

    self.window.rootViewController = self.rootVC;
    [self.window makeKeyAndVisible];
    
}

- (void)logoutInitiated:(NSNotification *)notification
{
    [self log:SFLogLevelDebug msg:@"Logout notification received.  Resetting app."];
    [self initializeAppViewState];
    [[SFAuthenticationManager sharedManager] loginWithCompletion:self.initialLoginSuccessBlock failure:self.initialLoginFailureBlock];
}

- (void)loginHostChanged:(NSNotification *)notification
{
    [self log:SFLogLevelDebug msg:@"Login host changed notification received.  Resetting app."];
//    [self massDeleteCoreData];
//    [self deleteAllUsers];
//    [self sfQueryRestaurant];
    [self initializeAppViewState];
    [[SFAuthenticationManager sharedManager] loginWithCompletion:self.initialLoginSuccessBlock failure:self.initialLoginFailureBlock];
}

-(TaBarViewController*)rootVC{

    if (_rootVC==nil) {
        
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        _rootVC = [storyboard instantiateInitialViewController];
        
        _rootVC = [storyboard instantiateViewControllerWithIdentifier:@"MainViewId"];
        
    }

    return _rootVC;
}

//Query restaurant from salesforce
-(void)sfQueryRestaurantMain{
    
    //    self.arrayOfretaurant = [[NSMutableArray alloc]init];
    NSLog(@"He Im here");
    self.queryRestaurant = YES;
    
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:@"SELECT id, Name, Restaurant_Id__c,  Cuisine__c, Rate__c, Image__c,Price_Range_End__c,Price_Range_Start__c, Type__c,Street__c, City__c,State__c,Country__c,Zip_Code__c FROM Restaurant__c WHERE Type__c = 'Main'"];
    [[SFRestAPI sharedInstance] send:request delegate:self];
}

//Query restaurant from salesforce
-(void)sfQueryRestaurant{
    
//    self.arrayOfretaurant = [[NSMutableArray alloc]init];
    NSLog(@"He Im here");
    self.queryRestaurant = YES;
    
//    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:@"SELECT id, Name, Restaurant_Id__c,  Cuisine__c, Rate__c, Image__c,Price_Range_End__c,Price_Range_Start__c, Type__c,Street__c, City__c,State__c,Country__c,Zip_Code__c, Restaurant_Main__r.Restaurant_Id__c FROM Restaurant__c WHERE Type__c = 'Branch'"];
    
    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:@"SELECT id, Name, Restaurant_Id__c,  Cuisine__c, Rate__c, Image__c,Price_Range_End__c,Price_Range_Start__c, Type__c,Street__c, City__c,State__c,Country__c,Zip_Code__c, Restaurant_Main__r.Restaurant_Id__c, (SELECT Name, Category__c, Menu_Id_Number__c,Menu_Image__c, Menu_Item_Details__c,Price__c, MenuImage_url__c FROM Menus__r) FROM Restaurant__c"];
    
    [[SFRestAPI sharedInstance] send:request delegate:self];
}

-(void)sfQueryAttachments{
    
    //    self.arrayOfretaurant = [[NSMutableArray alloc]init];
    NSLog(@"Downloading the attachements");
    _queryRestaurant = NO;
    
//    SFRestRequest *request = [[SFRestAPI sharedInstance] requestForQuery:@"SELECT Body,BodyLength,ContentType,CreatedById,CreatedDate,Description,Id,IsDeleted,IsPrivate,LastModifiedById,LastModifiedDate,Name,OwnerId,ParentId,SystemModstamp FROM Attachment"];
//    
//    [[SFRestAPI sharedInstance] send:request delegate:self];
//    SFRestRequest *request = [SFRestRequest requestWithMethod:SFRestMethodGET path:@"/services/data/v29.0/chatter/feeds/news/me/feed-items" queryParams:nil];
//    [[SFRestAPI sharedInstance] send:request delegate:self];
}


#pragma mark - SFRestAPIDelegate

- (void)request:(SFRestRequest *)request didLoadResponse:(id)jsonResponse {
    
    NSLog(@"%@",jsonResponse);
    if(_queryRestaurant){
        
        NSArray *records = [jsonResponse objectForKey:@"records"];
        NSLog(@"request:didLoadResponse: #records: %d", records.count);
        self.salesforceArray =  [[NSMutableArray alloc]init];
        NSMutableArray * arrayOfretaurant;
        arrayOfretaurant = records.mutableCopy;
        for (int i=0; i<arrayOfretaurant.count; i++) {
            NSDictionary *obj = [records objectAtIndex:i];
            Restaurant * resto = [NSEntityDescription insertNewObjectForEntityForName:@"Restaurant"
                                                               inManagedObjectContext:self.managedObjectContext];
            resto.name = [obj objectForKey:@"Name"];
            resto.restoId = [obj objectForKey:@"Restaurant_Id__c"];
            resto.cuisine = [obj objectForKey:@"Cuisine__c"];
            resto.type = [obj objectForKey:@"Type__c"];
            
            Menu * menu = [NSEntityDescription insertNewObjectForEntityForName:@"Menu"
                                                        inManagedObjectContext:self.managedObjectContext];
            NSMutableSet * menuItemSet = [NSMutableSet set];
            

            if([resto.type isEqualToString:@"Branch"]){
                
                NSDictionary *obj2 = [obj objectForKey:@"Restaurant_Main__r"];
                
                NSLog(@"SFPRestoMain: %@", [obj2 objectForKey:@"Restaurant_Id__c"]);
                resto.mainRestoId = [obj2 objectForKey:@"Restaurant_Id__c"];

            }else{
//                resto.image = @"about_us_r1_c1.jpg";
                resto.imageUrl = [obj objectForKey:@"Image__c"];
                NSLog(@"Image URL: %@", resto.imageUrl);
            }

            resto.rate = [MoizMenuUtil convertStringToNumber:[obj objectForKey:@"Rate__c"]];
            resto.priceRangeEnd = [MoizMenuUtil convertStringToNumber:[obj objectForKey:@"Price_Range_End__c"]];
            resto.priceRangeStart = [MoizMenuUtil convertStringToNumber:[obj objectForKey:@"Price_Range_Start__c"]];
            NSLog(@"SFName: %@", resto.name);
            if([obj objectForKey:@"Menus__r"] != nil){
                NSDictionary *dic = [obj objectForKey:@"Menus__r"];
                NSArray *menuRecords = [dic objectForKey:@"records"];
                NSMutableArray *menuArray = menuRecords.mutableCopy;
                
                for (int j=0; j<menuArray.count; j++) {
                    NSDictionary *menuObj = [menuArray objectAtIndex:j];
                    menu.restaurantId  = [obj objectForKey:@"Restaurant_Id__c"];
                    MenuItem * menuItem  = [NSEntityDescription insertNewObjectForEntityForName:@"MenuItem" inManagedObjectContext:self.managedObjectContext];
                    
                    NSLog(@"SFPRice: %@", [menuObj objectForKey:@"Price__c"]);
                    
                    menuItem.category = [menuObj objectForKey:@"Category__c"];
                    menuItem.menuId = [menuObj objectForKey:@"Menu_Id_Number__c"];
                    
                    if(![[menuObj objectForKey:@"MenuImage_url__c"] isEqualToString:@""])
                        menuItem.image = [menuObj objectForKey:@"MenuImage_url__c"];
                    menuItem.menu_item_details = [menuObj objectForKey:@"Menu_Item_Details__c"];
                    menuItem.name = [menuObj objectForKey:@"Name"];
                    menuItem.price = [MoizMenuUtil convertStringToDecimal:[menuObj objectForKey:@"Price__c"]];
                    
                    NSLog(@"SFMenuName: %@", menuItem.name);
                    [menuItemSet addObject:menuItem];
                }
                
            }
            
            resto.street = [obj objectForKey:@"Street__c"];
            resto.city = [obj objectForKey:@"City__c"];
            resto.state = [obj objectForKey:@"State__c"];
            resto.country = [obj objectForKey:@"Country__c"];
            resto.zipcode = [obj objectForKey:@"Zip_Code__c"];
            resto.originType = @"Salesforce";

            NSLog(@"SFName: %@", resto.name);
            NSLog(@"SFRestoID: %@", resto.restoId);
            NSLog(@"SFCuisine: %@", resto.cuisine);
            NSLog(@"SFRate: %@",resto.rate);
            NSLog(@"SFType: %@",resto.type);
            NSLog(@"SFPRiceRStart: %@",resto.priceRangeStart);
            NSLog(@"SFPRiceREnd: %@", resto.priceRangeEnd);
            
            menu.menuItem = menuItemSet;
            
            resto.menu = menu;
            
//            [self.salesforceArray addObject:resto.restoId];
            
        }
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
    }
//    else{
//        NSLog(@"%@",jsonResponse);
//        NSArray *records = [jsonResponse objectForKey:@"records"];
//        NSLog(@"request:didLoadResponse: #records: %d", records.count);
//    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
    });
    
}


- (void)request:(SFRestRequest*)request didFailLoadWithError:(NSError*)error {
    NSLog(@"request:didFailLoadWithError: %@", error);
    //add your failed error handling here
}

- (void)requestDidCancelLoad:(SFRestRequest *)request {
    NSLog(@"requestDidCancelLoad: %@", request);
    //add your failed error handling here
}

- (void)requestDidTimeout:(SFRestRequest *)request {
    NSLog(@"requestDidTimeout: %@", request);
    //add your failed error handling here
}

- (void) massDeleteCoreData{
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"Restaurant" inManagedObjectContext:self.managedObjectContext]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [self.managedObjectContext executeFetchRequest:allCars error:&error];
    //    [allCars release];
    //error handling goes here
    NSLog(@"Deleting core data from restaurant");
    for (NSManagedObject * car in cars) {
        [self.managedObjectContext deleteObject:car];
    }
    NSError *saveError = nil;
    [self.managedObjectContext save:&saveError];
    
    
    [self deleteAllMenu];
    [self deleteAllMenuItem];
    
}

- (void) deleteAllMenu{
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"Menu" inManagedObjectContext:self.managedObjectContext]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [self.managedObjectContext executeFetchRequest:allCars error:&error];
    //    [allCars release];
    //error handling goes here
    NSLog(@"Deleting core data menu");
    for (NSManagedObject * car in cars) {
        [self.managedObjectContext deleteObject:car];
    }
    NSError *saveError = nil;
    [self.managedObjectContext save:&saveError];
    
}

- (void) deleteAllMenuItem{
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"MenuItem" inManagedObjectContext:self.managedObjectContext]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [self.managedObjectContext executeFetchRequest:allCars error:&error];
    //    [allCars release];
    //error handling goes here
    NSLog(@"Deleting core data menuitem");
    for (NSManagedObject * car in cars) {
        [self.managedObjectContext deleteObject:car];
    }
    NSError *saveError = nil;
    [self.managedObjectContext save:&saveError];
    
    
}

- (void) deleteAllUsers{
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:@"User" inManagedObjectContext:self.managedObjectContext]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [self.managedObjectContext executeFetchRequest:allCars error:&error];
    //    [allCars release];
    //error handling goes here
    NSLog(@"Deleting core data users");
    for (NSManagedObject * car in cars) {
        [self.managedObjectContext deleteObject:car];
    }
    NSError *saveError = nil;
    [self.managedObjectContext save:&saveError];
    
    
}

- (void)registerDefaultsFromSettingsBundle
{
    //    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if(!settingsBundle) {
        NSLog(@"Could not find Settings.bundle");
        return;
    }
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    NSLog(@"Preferences : %@", preferences);
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    for(NSDictionary *prefSpecification in preferences) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        
        if(key)
        {
//            id obj = [NSUserDefaults objectForKey:key];
//            NSLog(@"key %@ : %@", key, obj);
            //            if (!obj || ([obj isKindOfClass:[NSString class]] && ((NSString *)obj).length == 0))
            //            {
            if ([[prefSpecification allKeys] containsObject:@"DefaultValue"])
            {
//                [NSUserDefaults setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
                
                NSString *strKey = key;
                NSString *strPrepSpec = [prefSpecification objectForKey:@"DefaultValue"];
                
                NSLog(@"SETTT %@ : %@", strKey, strPrepSpec);
                
                if([strKey isEqualToString:@"local_login"]){
                    if(strPrepSpec == 0){
                        NSLog(@"Local login is enabeled");
                    }
                }

                [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
            }

        }
    }
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (ChatterRequestHelper *)chatterRequestHelper
{
    if (!_chatterRequestHelper)
        _chatterRequestHelper = [ChatterRequestHelper sharedInstance];
    return _chatterRequestHelper;
}





@end
