//
//  ChatterUser.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/27/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatterUser : NSObject

@property (strong, nonatomic) NSDictionary *userData;
@property (strong, nonatomic) NSString *companyName;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSURL *smallProfileImageURL;
@property (strong, nonatomic) UIImage *smallProfileImage;
@property (strong, nonatomic) NSString *profileImageFullPath;
@property (strong, nonatomic) NSString *userID;
-(id)initWithUserDict:(NSDictionary*)userDict;

@end
