//
//  RestaurantBranchTableCell.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/14/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantBranchTableCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *restoNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *addressLabel;
@property (nonatomic, strong) IBOutlet UIImageView *restoMainImage;

@end
