//
//  ChatterImage.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/26/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "ChatterImage.h"
#import "SFOAuthCoordinator.h"
#import "SFRestAPI.h"
#import "MoizMenuUtil.h"

@interface ChatterImage(){
    
}

@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *data;

@end

@implementation ChatterImage

-(void)downloadSmallProfileImageAsyncFromURL:(NSURL*)url {
    
    NSMutableURLRequest *req = [MoizMenuUtil getAuthenticatedRequestWithURL:url];
    _connection = [NSURLConnection connectionWithRequest:req delegate:self];
    [_connection start];
}

+ (NSMutableURLRequest *)getAuthenticatedRequestWithURL:(NSURL *)url
{
    SFOAuthCredentials *creds = [[SFRestAPI sharedInstance] coordinator].credentials;
    if (creds != nil) {
        NSString *value = [NSString stringWithFormat:@"OAuth %@", creds.accessToken];
        NSDictionary *headers = [NSDictionary dictionaryWithObject:value forKey:@"Authorization"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        for (NSString *header in headers)
        {
            NSString *value = [headers objectForKey:header];
            [request addValue:value forHTTPHeaderField:header];
        }
        [request setHTTPMethod:@"GET"];
        return request;
    } else {
        return nil;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	_data = [NSMutableData dataWithCapacity:1024];
	[_data setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)inData {
	[_data appendData:inData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *fullPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[MoizMenuUtil getUniqueId]]];
    
    [_data writeToFile:fullPath atomically:YES];
    
//	self.image = [UIImage imageWithData:_data];
    //self.profileImageFullPath = fullPath;
	_data = nil;
	connection = nil;
    
    //DLog(@"File Path when saved: %@",self.profileImageFullPath);
}


@end
