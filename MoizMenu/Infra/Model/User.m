//
//  User.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/25/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic userName;
@dynamic profile;
@dynamic firstName;
@dynamic lastName;
@dynamic email;
@dynamic status;
@dynamic pictureUrl;

@end
