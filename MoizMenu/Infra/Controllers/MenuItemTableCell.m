//
//  MenuItemTableCell.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/5/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "MenuItemTableCell.h"

@implementation MenuItemTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
