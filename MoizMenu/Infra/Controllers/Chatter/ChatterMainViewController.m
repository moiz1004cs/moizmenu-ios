//
//  ChatterMainViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/27/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "ChatterMainViewController.h"
#import "ChatterViewController.h"

@interface ChatterMainViewController ()

@property (nonatomic, strong) ChatterViewController *chatterView;

@end

@implementation ChatterMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //load RestaurnatViewController
    _chatterView = [[ChatterViewController alloc]initWithNibName:nil bundle:nil];
    //    [_restaurantViewController setDelegate:self];
    [_mainView addSubview:_chatterView.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
