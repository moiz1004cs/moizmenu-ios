//
//  GMSLocation.h
//  NYLife
//
//  Created by Matthew Casey on 05/09/2013.
//  Copyright (c) 2013 CloudSherpas. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef struct {
    float latitude;
    float longitude;
} LatLng;
@interface GMSLocation : NSObject
@property (nonatomic) LatLng latLng;
@property (nonatomic, strong) NSString *address;
@property (nonatomic) BOOL useAddress;
+(GMSLocation *)locationWithAddress:(NSString *)aAddress;
+(GMSLocation *)locationWithLatitude:(float)lat longitude:(float)lng;
-(NSString *)stringFromLocation;
@end
