//
//  MenuItemTableCell.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/5/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuItemTableCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *menuItemName;
@property (nonatomic, strong) IBOutlet UILabel *menuItemPrice;
@property (nonatomic, strong) IBOutlet UILabel *menuItemCategory;
@property (nonatomic, strong) IBOutlet UIImageView *menuItemImage;

@end
