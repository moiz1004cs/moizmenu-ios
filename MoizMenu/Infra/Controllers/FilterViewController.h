//
//  FilterViewController.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/11/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterViewDelegate <NSObject>

@optional
-(void)plotFilters:(NSString *)restoName cuisine:(NSString*) cusineName priceRage:(NSString *)pricerange;
-(void)closePlotDirection;
@end

@interface FilterViewController : UIViewController<UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource>{
    NSMutableArray *cuisneArray;
    NSMutableArray *priceRangeArrayStart;
    NSMutableArray *priceRangeArrayEnd;
}

@property (strong, nonatomic) IBOutlet UITextField *restoName;
@property (strong, nonatomic) IBOutlet UITextField *cusineName;
@property (strong, nonatomic) IBOutlet UITextField *priceRangeStart;
@property (strong, nonatomic) IBOutlet UITextField *priceRangeEnd;

@property (weak, nonatomic) IBOutlet UIButton * cuisineButton;
@property (weak, nonatomic) IBOutlet UIButton * priceRangeStartButton;
@property (weak, nonatomic) IBOutlet UIButton * priceRangeENdButton;
@property (weak, nonatomic) IBOutlet UIButton *submitFilterButton;

@property (strong, nonatomic) IBOutlet UIPickerView * cuisinePickerView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;


@property (nonatomic, assign) id<FilterViewDelegate> delegate;

@property (nonatomic,retain)NSMutableArray *cuisineArray;
@property (nonatomic,retain)NSMutableArray *priceRangeArrayStart;
@property (nonatomic,retain)NSMutableArray *priceRangeArrayEnd;

@property (strong, nonatomic) NSMutableArray *restaurantArray;

@end
