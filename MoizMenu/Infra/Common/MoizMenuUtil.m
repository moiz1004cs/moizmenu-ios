//
//  MoizMenuUtil.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/4/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "MoizMenuUtil.h"
#import "MoizMenuStyle.h"
#import "SFOAuthCredentials.h"
#import "SFRestAPI.h"
#import "SFAccountManager.h"
//#import "NSData+Base64.h"
@interface MoizMenuUtil(){
    
}
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *data;
@end


@implementation MoizMenuUtil

+ (CGFloat)windowHeight{
    
    return [UIScreen mainScreen].applicationFrame.size.height;
    
}

+ (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+ (BOOL)hasInternetConnection
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr*)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    if (!didRetrieveFlags) {
        NSLog(@"Error. Could not recover network reachability flags");
        return 0;
    }
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    
    BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
    NSURL *testURL = [NSURL URLWithString:@"http://www.apple.com/"];
    NSURLRequest *testRequest = [NSURLRequest requestWithURL:testURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0];
    NSURLConnection *testConnection = [[NSURLConnection alloc] initWithRequest:testRequest delegate:nil];
    
    return ((isReachable && !needsConnection) || nonWiFi) ? (testConnection ? YES : NO) : NO;
}

+ (UILabel*)titleViewWithTitleOnly:(NSString*)strTitle{
    
    UILabel *lblTitle = [[UILabel alloc] init];
    [lblTitle setText:strTitle];
    [lblTitle setBackgroundColor:[UIColor clearColor]];
    [lblTitle setTextAlignment:NSTextAlignmentCenter];

        [lblTitle setTextColor:[UIColor whiteColor]];
        [lblTitle setFrame:CGRectMake(0, 0, 150, 44)];
        [lblTitle setFont:kFONT_STYLE_SEGOEUI_20];

    return lblTitle;
}

+ (NSNumber *)convertStringToNumber:(NSString *)strNumber{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber * myNumber = [f numberFromString:strNumber];
    
    return myNumber;
}

+ (NSData *)converBase64toImage:(NSString *)stringImage
{
    
    NSData *imageData = [self dataFromBase64EncodedString:stringImage];
//    UIImage *myImage = [UIImage imageWithData:imageData];
    return imageData;
}
+(NSData *)dataFromBase64EncodedString:(NSString *)string{
    if (string.length > 0) {
        
        //the iPhone has base 64 decoding built in but not obviously. The trick is to
        //create a data url that's base 64 encoded and ask an NSData to load it.
        NSString *data64URLString = [NSString stringWithFormat:@"data:;base64,%@", string];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:data64URLString]];
        return data;
    }
    return nil;
}


+ (NSDecimalNumber *)convertStringToDecimal:(NSString *)strNumber{
//    NSString *order = @"";
    
    NSString * str = [NSString stringWithFormat:@"%@.00",strNumber];

//    NSDecimalNumber *price;
//    price = [NSDecimalNumber decimalNumberWithMantissa:strNumber
//                                              exponent:-2
//                                            isNegative:NO];
//    price = [NSDecimalNumber decimalNumberWithString:@"15.99"];
    
    
//    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    
//    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    
    NSDecimalNumber *decimal = [NSDecimalNumber decimalNumberWithString:str];
    
//
//    [formatter setLocale:locale];
    
//    return [NSNumberFormatter localizedStringFromNumber:decimal numberStyle:NSNumberFormatterDecimalStyle];
    return decimal;
}

+ (NSMutableURLRequest *)getAuthenticatedRequestWithURL:(NSURL *)url
{
    SFOAuthCredentials *creds = [[SFRestAPI sharedInstance] coordinator].credentials;
    if (creds != nil) {
        NSString *value = [NSString stringWithFormat:@"OAuth %@", creds.accessToken];
        NSDictionary *headers = [NSDictionary dictionaryWithObject:value forKey:@"Authorization"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        for (NSString *header in headers)
        {
            NSString *value = [headers objectForKey:header];
            [request addValue:value forHTTPHeaderField:header];
        }
        [request setHTTPMethod:@"GET"];
        return request;
    } else {
        return nil;
    }
}

+ (NSData *)getImageFromURL:(NSString *)url{
    NSURL * imageURL = [NSURL URLWithString:url];
    NSMutableURLRequest *urlMutable = [MoizMenuUtil getAuthenticatedRequestWithURL:imageURL];
    NSData *imageData = [NSURLConnection sendSynchronousRequest:urlMutable returningResponse:nil error:nil];
    UIImage *image = [[UIImage alloc] initWithData:imageData];
    NSData *data2 = [NSData dataWithData:UIImageJPEGRepresentation(image, 1.0f)];//1.0f = 100% quality
    
    return data2;

}

+ (NSData *)getImageFromURLLink:(NSURL *)url{
//    NSURL * imageURL = [NSURL URLWithString:url];
    NSMutableURLRequest *urlMutable = [MoizMenuUtil getAuthenticatedRequestWithURL:url];
    NSData *imageData = [NSURLConnection sendSynchronousRequest:urlMutable returningResponse:nil error:nil];
    UIImage *image = [[UIImage alloc] initWithData:imageData];
    NSData *data2 = [NSData dataWithData:UIImageJPEGRepresentation(image, 1.0f)];//1.0f = 100% quality
    
    return data2;
    
}

- (void)downloadSmallProfileImageAsyncFromURL:(NSURL*)url {
    
    NSMutableURLRequest *req = [MoizMenuUtil getAuthenticatedRequestWithURL:url];
    _connection = [NSURLConnection connectionWithRequest:req delegate:self];
    [_connection start];
}

+ (NSString *)getUniqueId
{
    NSString* uniqueId = @"";
    NSDate *date = [NSDate date]; //current date
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYYMMDDhhmmss"];
    
    uniqueId = [NSString stringWithFormat:@"%@%d%f", [formatter stringFromDate:date], arc4random(), [date timeIntervalSinceNow]];
    
    return uniqueId;
}



@end
