//
//  RestaurantDetailViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/14/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "RestaurantDetailViewController.h"
#import "RestaurantBranchTableCell.h"
#import "StoreMenuViewController.h"
#import "AppleMapViewController.h"
#import "RestaurantViewController.h"
#import "AppDelegate.h"
#import "MoizMenuUtil.h"
#import "MoizMenuStyle.h"
#define APP_DELEGATE                    ((AppDelegate *)[[UIApplication sharedApplication] delegate])
@interface RestaurantDetailViewController ()


@property (nonatomic, strong) StoreMenuViewController *storeMenuViewController;
@property (nonatomic, strong) AppleMapViewController *mapController;
@property (nonatomic, strong) RestaurantViewController *restaurantViewController;
@property (nonatomic, strong) NSMutableArray* fetchedRecordsArray;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@property (nonatomic) BOOL sortBranch;

@end

@implementation RestaurantDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationTitle.title = self.selectedRestaurant.name;
    self.cuisine.text = self.selectedRestaurant.cuisine;
    self.address.text = [NSString stringWithFormat:@"%@ %@, %@ %@", self.selectedRestaurant.street, self.selectedRestaurant.city, self.selectedRestaurant.zipcode, self.selectedRestaurant.country];
    self.priceRange.text = [NSString stringWithFormat:@"%@ - %@", _selectedRestaurant.priceRangeStart, _selectedRestaurant.priceRangeEnd];
    self.type.text = self.selectedRestaurant.type;

    
    if([self.selectedRestaurant.rate  isEqual: @5])
        _ratingImage.image = [UIImage imageNamed:@"5-stars.png"];
    if([self.selectedRestaurant.rate  isEqual: @4])
        _ratingImage.image = [UIImage imageNamed:@"4-stars.png"];
    if([self.selectedRestaurant.rate  isEqual: @3])
        _ratingImage.image = [UIImage imageNamed:@"3-stars.png"];
    if([self.selectedRestaurant.rate  isEqual: @2])
        _ratingImage.image = [UIImage imageNamed:@"2-stars.png"];
    if([self.selectedRestaurant.rate  isEqual: @1])
        _ratingImage.image = [UIImage imageNamed:@"1-star.png"];
    
    NSString * str = self.selectedRestaurant.restoId;
    
    NSLog(@"Query all the branches of: %@", str);
    [self.branchesLabelButton setTitle:@"Branches ↓" forState:UIControlStateNormal];
    self.sortBranch = YES;

    self.fetchedRecordsArray = [[APP_DELEGATE getAllBranchesByRestoId:self.selectedRestaurant.restoId]mutableCopy];

    [self.branchesLabelButton addTarget:self action:@selector(sortBranches) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *showMenu = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStylePlain target:self action:@selector(goToMenu)];
    
//    UIBarButtonItem *showMap = [[UIBarButtonItem alloc] initWithTitle:@"Loc" style:UIBarButtonItemStylePlain target:self action:@selector(showOnMap)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:showMenu,nil];
    [self.navigationController setNavigationBarHidden:NO];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
//
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
//    return [menuItemSet count];
        return [self.fetchedRecordsArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RestaurantBranchTableCell";
    
    RestaurantBranchTableCell *cell = (RestaurantBranchTableCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil){
        cell = [[RestaurantBranchTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"RestaurantBranchTableCell" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[RestaurantBranchTableCell class]])
        {
            cell = (RestaurantBranchTableCell *)currentObject;
            break;
        }
    }
    
    Restaurant * record = [self.fetchedRecordsArray objectAtIndex:indexPath.row];
    
    NSLog(@"Loading to cell %@",record.name);
    
    cell.restoNameLabel.text = record.name;
    cell.addressLabel.text = [NSString stringWithFormat:@"%@ %@, %@ %@", record.street, record.city, record.zipcode, record.country];
//    cell.restoMainImage.image = [UIImage imageNamed:self.selectedRestaurant.image];
    
    if([record.type isEqualToString:@"Branch"]){
        if([record.originType isEqualToString:@"Salesforce"]){
            cell.restoMainImage.image = [UIImage imageWithData:[MoizMenuUtil getImageFromURL:[APP_DELEGATE getMainBranchImage:record.mainRestoId]]];
        }else{
            cell.restoMainImage.image = [UIImage imageNamed:[APP_DELEGATE getMainBranchImage:record.mainRestoId]];
        }
        
    }else{
        if([record.originType isEqualToString:@"Salesforce"]){
            
            cell.restoMainImage.image = [UIImage imageWithData:[MoizMenuUtil getImageFromURL:record.imageUrl]];
        }else{
            cell.restoMainImage.image = [UIImage imageNamed:record.image];
        }
        
    }

    return cell;
}


#pragma mark UITableViewDelegate functions
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_storeMenuViewController)
        _storeMenuViewController = nil;
    
    _storeMenuViewController = [[StoreMenuViewController alloc] initWithNibName:@"StoreMenuViewController" bundle:nil];
    _storeMenuViewController.selectedRestaurant = [self.fetchedRecordsArray objectAtIndex:indexPath.row];

    [self.navigationController pushViewController:_storeMenuViewController animated:YES];
    

}

-(void)goToMenu{
    
//    float height = [MoizMenuUtil windowHeight] - (64*2);
    _storeMenuViewController = [[StoreMenuViewController alloc] initWithNibName:@"StoreMenuViewController" bundle:nil];
    _storeMenuViewController.selectedRestaurant = self.selectedRestaurant;
    _storeMenuViewController.origin = RESTAURANT_DETAIL_VIEW_ORIGIN;
    //    [self. rightViewPushViewControllerOverCenterController:_storeMenuViewController];
    
    [self.navigationController pushViewController:_storeMenuViewController animated:YES];
    
}
-(IBAction)goBack:(id)sender{
    
    if([self.origin isEqualToString:MAP_ORIGIN]){
        _mapController = [[AppleMapViewController alloc] initWithNibName:@"AppleMapViewController" bundle:nil];
//        _storeMenu.selectedRestaurant = resto;
        [self.navigationController pushViewController:_mapController animated:YES];
    }else if([self.origin isEqualToString:RESRAURANT_MAIN]){
        
        float height = [MoizMenuUtil windowHeight] - (64*2);
        _restaurantViewController = [[RestaurantViewController alloc] initWithNibName:@"RestaurantViewController" bundle:nil];
//        _restaurantViewController.selectedRestaurant = self.selectedRestaurant;
        _restaurantViewController.origin = RESTAURANT_DETAIL_VIEW_ORIGIN;
        //    [self. rightViewPushViewControllerOverCenterController:_storeMenuViewController];
        
        [self.view addSubview:_restaurantViewController.view];
        
        
        [_restaurantViewController.view setFrame:CGRectMake(320,0, 320, height)];
        
        NSLog(@"rect %@",NSStringFromCGRect(_restaurantViewController.view.frame));
        
        [UIView animateWithDuration:0.3 animations:^{
            [_restaurantViewController.view setFrame:CGRectMake(0,0, 320, height)];
        } completion:^(BOOL isDone){
            
        }];
        
//        _restaurantViewController = [[RestaurantViewController alloc] initWithNibName:@"RestaurantViewController" bundle:nil];
//        [self.navigationController pushViewController:_restaurantViewController animated:YES];
    }
    
}

-(void)sortBranches{
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    [tempArray removeAllObjects];
    [tempArray addObjectsFromArray: _fetchedRecordsArray];
    NSString *key = @"name";
    
    if(self.sortBranch){
        [self.branchesLabelButton setTitle:@"Branches ↓" forState:UIControlStateNormal];
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        
        self.sortBranch = NO;
    }else{
        [self.branchesLabelButton setTitle:@"Branches ↑" forState:UIControlStateNormal];
        NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:brandDescriptor,nil];
        NSArray *sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        //    [brandDescriptor release];
        [tempArray removeAllObjects];
        tempArray = (NSMutableArray*)sortedArray;
        
        self.sortBranch = YES;
    }
    [_fetchedRecordsArray removeAllObjects];
    [_fetchedRecordsArray addObjectsFromArray:tempArray];
    [_tableView reloadData];

    
}


@end
