//
//  SettingViewController.m
//  MoizMenu
//
//  Created by Abdul Esmail on 2/19/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import "SettingViewController.h"
#import "SFAccountManager.h"
#import "SFAuthenticationManager.h"
#import "SFPushNotificationManager.h"
#import "SFOAuthInfo.h"
#import "SFLogger.h"
#import "MoizMenuUtil.h"
#import "AppDelegate.h"
#define APP_DELEGATE                    ((AppDelegate *)[[UIApplication sharedApplication] delegate])

// Fill these in when creating a new Connected Application on Force.com
static NSString * const RemoteAccessConsumerKey = @"3MVG9Y6d_Btp4xp5wCSX4B.3qSokhkAjuStIQdL2jAvmk.Mb6OlO9xfPFje1boi1QKiGgf5XkywHVst1r1Vla";
static NSString * const OAuthRedirectURI        = @"sfdc://success";

@interface SettingViewController ()

@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *data;

@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    User *user = [APP_DELEGATE getUserProfile];
    
    if(user){
        self.fullName.text = [NSString stringWithFormat:@"%@, %@",user.lastName, user.firstName];
        self.userName.text = user.userName;
        self.profile.text = user.profile;
        self.email.text = user.email;
        NSURL *imageURL = [NSURL URLWithString:user.pictureUrl];
        [self downloadSmallProfileImageAsyncFromURL:imageURL];
    }else{
        self.imagePrfile.image = [UIImage imageNamed:@"default_person.png"];
        [self.view insertSubview:_loginBUtton atIndex:0];
//        [self.loginBUtton setTitle:@"" forState:UIControlStateNormal];
    }
    //load the user's profile
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)logout {
    //[[SFAuthenticationManager sharedManager] logout];
    //[DEFAULTS removeObjectForKey:kUserProfile];
    //[DEFAULTS removeObjectForKey:PROFILE_TYPE_KEY];
    [SFAuthenticationManager removeAllCookies];
    [[SFAccountManager sharedInstance] clearAccountState:YES];
}

-(IBAction)sync:(id)sender{
    if([MoizMenuUtil hasInternetConnection]){
        NSLog(@"Synching");
        [APP_DELEGATE massDeleteCoreData];
        [APP_DELEGATE sfQueryRestaurant];
    }
}

#pragma mark New Salesforce SDK stuff
-(IBAction)appLogout:(id)sender{
    //[self logoutInitiated:nil];
    [[SFAuthenticationManager sharedManager] logout];

}
- (void)appLogout
{
    //[self logoutInitiated:nil];
    [[SFAuthenticationManager sharedManager] logout];
}

-(void)downloadSmallProfileImageAsyncFromURL:(NSURL*)url {
    
    NSMutableURLRequest *req = [MoizMenuUtil getAuthenticatedRequestWithURL:url];
    _connection = [NSURLConnection connectionWithRequest:req delegate:self];
    [_connection start];
}

+ (NSMutableURLRequest *)getAuthenticatedRequestWithURL:(NSURL *)url
{
    SFOAuthCredentials *creds = [[SFRestAPI sharedInstance] coordinator].credentials;
    if (creds != nil) {
        NSString *value = [NSString stringWithFormat:@"OAuth %@", creds.accessToken];
        NSDictionary *headers = [NSDictionary dictionaryWithObject:value forKey:@"Authorization"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        for (NSString *header in headers)
        {
            NSString *value = [headers objectForKey:header];
            [request addValue:value forHTTPHeaderField:header];
        }
        [request setHTTPMethod:@"GET"];
        return request;
    } else {
        return nil;
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	_data = [NSMutableData dataWithCapacity:1024];
	[_data setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)inData {
	[_data appendData:inData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *fullPath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[MoizMenuUtil getUniqueId]]];
    
    [_data writeToFile:fullPath atomically:YES];
//    if(_data){
        self.imagePrfile.image = [UIImage imageWithData:_data];
//    }else{
//        self.imagePrfile.image = [UIImage imageNamed:@"default_person.png"];
//    }
    //self.profileImageFullPath = fullPath;
	_data = nil;
	connection = nil;
    
    //DLog(@"File Path when saved: %@",self.profileImageFullPath);
}


@end
