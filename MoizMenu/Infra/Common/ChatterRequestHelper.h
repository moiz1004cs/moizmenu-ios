//
//  ChatterRequestHelper.h
//  MoizMenu
//
//  Created by Abdul Esmail on 2/27/14.
//  Copyright (c) 2014 Abdulmoiz Esmail. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SFRestAPI.h"

@interface ChatterRequestHelper : NSObject<SFRestDelegate>


@property (strong, nonatomic) NSMutableArray *feedResults;
@property (strong, nonatomic) NSMutableArray *meResults;

+ (ChatterRequestHelper *)sharedInstance;

-(void)getChatterItems;
-(void)getMeItems;

-(NSMutableArray*)returnChatterItems;
-(NSMutableArray*)returnMeItems;


@end
